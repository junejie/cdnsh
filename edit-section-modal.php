<div id="editModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="submit" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Edit Section</h4>
			</div>
			<div class="modal-body">
				<div class="alert alert-warning">Update the info of this section</div>
				<div class="modal-footer">
					<form class="form-inline" method="POST" action="process.php">

						<input type="text" class="form-control" id="secName" placeholder="Section Name" value="<?php echo $row['code-section']; ?>">
						<input type="text" class="form-control" id="gradelvl" placeholder="Grade Level" value="<?php echo $row['title-section']; ?>">
						
						<button type="submit" class="btn btn-primary" name="btnEditSection" id="btnEditSection">Edit</button>
						<button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
					</form>
				</div>
			</div>
		</div>

	</div>
</div>
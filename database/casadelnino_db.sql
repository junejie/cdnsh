-- phpMyAdmin SQL Dump
-- version 4.5.3.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 19, 2016 at 09:09 PM
-- Server version: 5.5.46-0ubuntu0.14.04.2
-- PHP Version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `casadelnino_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `account_type`
--

CREATE TABLE `account_type` (
  `id` int(11) NOT NULL,
  `user_type` varchar(30) NOT NULL,
  `group_user` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `account_type`
--

INSERT INTO `account_type` (`id`, `user_type`, `group_user`) VALUES
(1, 'Student', 'student'),
(2, 'Principal', 'admin'),
(3, 'Admin IT Professor', 'admin'),
(4, 'Teachers', 'faculty'),
(5, 'Registrar', 'faculty'),
(6, 'Faculty IT Professor', 'faculty'),
(7, 'Superuser', 'superuser');

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE `activities` (
  `id` int(11) NOT NULL,
  `activity_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `activity_to_year` int(11) NOT NULL,
  `activity_to_section` int(11) NOT NULL,
  `activity_by` int(11) NOT NULL,
  `content` text NOT NULL,
  `docname` varchar(100) NOT NULL,
  `filename` varchar(100) NOT NULL,
  `status` enum('open','close') NOT NULL DEFAULT 'open'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activities`
--

INSERT INTO `activities` (`id`, `activity_date`, `activity_to_year`, `activity_to_section`, `activity_by`, `content`, `docname`, `filename`, `status`) VALUES
(33, '2016-02-11 00:00:00', 1, 3, 47, '', 'Data _ Wikipedia, the free encyclopedia.pdf', 'phpGxsjpc', 'open'),
(37, '2016-02-11 00:00:00', 1, 3, 47, '', 'Data _ Wikipedia, the free encyclopedia.pdf', 'phplAdRe9', 'open'),
(38, '2016-02-18 16:52:52', 1, 3, 16, '', 'Data _ Wikipedia, the free encyclopedia.pdf', 'php2Ci9nk', 'open'),
(39, '2016-02-19 00:00:00', 1, 3, 14, 'o', 'Data _ Wikipedia, the free encyclopedia.pdf', 'phpJJQju5', 'close'),
(40, '2016-02-27 00:00:00', 1, 3, 35, '1', 'Data _ Wikipedia, the free encyclopedia.pdf', 'phpUP5PE4', 'close'),
(41, '2016-03-11 00:00:00', 1, 3, 1, '1', 'Data _ Wikipedia, the free encyclopedia.pdf', 'php4Ui96R', 'open'),
(42, '2016-03-18 00:00:00', 5, 7, 2, '111', 'Data _ Wikipedia, the free encyclopedia.pdf', 'phpaX7z2G', 'open');

-- --------------------------------------------------------

--
-- Table structure for table `activities_answer`
--

CREATE TABLE `activities_answer` (
  `id` int(11) NOT NULL,
  `fk_activity` int(11) NOT NULL,
  `fk_studentid` int(11) NOT NULL,
  `docname` varchar(100) NOT NULL,
  `filename` varchar(100) NOT NULL,
  `ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activities_answer`
--

INSERT INTO `activities_answer` (`id`, `fk_activity`, `fk_studentid`, `docname`, `filename`, `ts`) VALUES
(30, 33, 3, 's..jpg.out.pp3', 'phpljRKaR', '2016-03-19 06:53:43');

-- --------------------------------------------------------

--
-- Table structure for table `announcement`
--

CREATE TABLE `announcement` (
  `id` int(11) NOT NULL,
  `announcement_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `announce_to_year` int(11) NOT NULL,
  `announce_to_section` int(11) NOT NULL,
  `announce_by` int(11) NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `announcement`
--

INSERT INTO `announcement` (`id`, `announcement_date`, `announce_to_year`, `announce_to_section`, `announce_by`, `content`) VALUES
(4, '2016-02-11 00:00:00', 1, 4, 1, '1'),
(5, '2016-02-11 00:00:00', 1, 3, 2, '1'),
(8, '2016-02-27 00:00:00', 1, 3, 16, 'qqqq'),
(9, '2016-02-25 00:00:00', 1, 3, 47, 'ww'),
(10, '2016-02-26 00:00:00', 1, 3, 35, '1'),
(11, '2016-02-26 00:00:00', 1, 3, 14, 'a');

-- --------------------------------------------------------

--
-- Table structure for table `calendar`
--

CREATE TABLE `calendar` (
  `id` int(11) NOT NULL,
  `startdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `enddate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `to_year` int(11) NOT NULL,
  `to_section` int(11) NOT NULL,
  `saved_by` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `content` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `calendar`
--

INSERT INTO `calendar` (`id`, `startdate`, `enddate`, `to_year`, `to_section`, `saved_by`, `title`, `content`) VALUES
(5, '2016-02-11 00:00:00', '0000-00-00 00:00:00', 1, 3, 2, 'yyyy', '1'),
(8, '2016-02-27 00:00:00', '0000-00-00 00:00:00', 1, 3, 16, 'uuuu', 'qqqq'),
(9, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 16, '', ''),
(10, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 0, 16, '', ''),
(15, '2016-03-09 00:00:00', '2016-03-10 00:00:00', 0, 0, 16, 'a', 'aa'),
(17, '2016-03-07 00:00:00', '2016-03-08 00:00:00', 0, 0, 16, 'rr rrrr', ' 00s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets co'),
(18, '2016-03-01 16:00:00', '2016-03-01 16:30:00', 0, 0, 16, 'w', 'ww');

-- --------------------------------------------------------

--
-- Table structure for table `grade_setting`
--

CREATE TABLE `grade_setting` (
  `id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `isenable` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grade_setting`
--

INSERT INTO `grade_setting` (`id`, `teacher_id`, `isenable`) VALUES
(291, 50, 1),
(292, 46, 1),
(293, 3, 1),
(294, 45, 1),
(295, 1, 1),
(296, 2, 1),
(297, 14, 1),
(298, 47, 1),
(299, 49, 1),
(300, 35, 1),
(301, 16, 1),
(302, 5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `lectures`
--

CREATE TABLE `lectures` (
  `id` int(11) NOT NULL,
  `saved_ts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `yearid` int(11) NOT NULL,
  `sectionid` int(11) NOT NULL,
  `subjectid` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `added_by` int(11) NOT NULL,
  `docname` varchar(200) NOT NULL,
  `filename` varchar(200) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lectures`
--

INSERT INTO `lectures` (`id`, `saved_ts`, `yearid`, `sectionid`, `subjectid`, `title`, `added_by`, `docname`, `filename`, `status`) VALUES
(13, '2016-02-05 00:00:00', 1, 3, 7, 'xxxxxxxxxxxx xxxxxxxxxxxxxx', 47, 'Data _ Wikipedia, the free encyclopedia.pdf', 'phpXCiPLM', 1),
(19, '2016-02-18 00:00:00', 5, 3, 15, 'A', 14, 'Data _ Wikipedia, the free encyclopedia.pdf', 'phpdGIWQW', 1),
(20, '2016-02-12 00:00:00', 5, 6, 15, 'f', 16, 'Data _ Wikipedia, the free encyclopedia.pdf', 'phpBmTTF7', 1),
(23, '2016-02-18 00:00:00', 4, 3, 37, 'q', 35, 'Data _ Wikipedia, the free encyclopedia.pdf', 'phpJjJHgd', 1),
(24, '2016-02-27 00:00:00', 5, 3, 18, '1', 35, 'Data _ Wikipedia, the free encyclopedia.pdf', 'phpQgQ1gM', 1),
(26, '2016-03-05 00:00:00', 2, 4, 7, 'y', 35, 'Data _ Wikipedia, the free encyclopedia.pdf', 'phpN383jW', 1),
(28, '2016-03-16 00:00:00', 1, 6, 27, '1', 35, 'Data _ Wikipedia, the free encyclopedia.pdf', 'phpM80dm6', 1),
(30, '2016-03-17 00:00:00', 5, 17, 15, '1', 1, 'Data _ Wikipedia, the free encyclopedia.pdf', 'phpa8XVHK', 1),
(31, '2016-03-18 00:00:00', 6, 21, 19, '1', 2, 'Data _ Wikipedia, the free encyclopedia.pdf', 'phpgaGiKI', 1);

-- --------------------------------------------------------

--
-- Table structure for table `schedule`
--

CREATE TABLE `schedule` (
  `id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `grade_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `schedule`
--

INSERT INTO `schedule` (`id`, `teacher_id`, `grade_id`, `section_id`, `subject_id`, `start_time`, `end_time`) VALUES
(86, 14, 2, 8, 31, '00:30:00', '01:00:00'),
(88, 14, 2, 7, 7, '00:30:00', '01:30:00'),
(89, 47, 1, 3, 1, '00:30:00', '01:00:00'),
(90, 14, 2, 7, 31, '00:30:00', '01:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `setting`
--

CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `setting_key` varchar(50) NOT NULL,
  `setting_val` varchar(50) NOT NULL,
  `setting_type` enum('tbi') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `student_grade`
--

CREATE TABLE `student_grade` (
  `id` int(11) NOT NULL,
  `fk_student_id` int(11) NOT NULL,
  `fk_subject_id` int(11) NOT NULL,
  `fk_teacher_id` int(11) NOT NULL,
  `grading_1` int(2) NOT NULL,
  `grading_2` int(2) NOT NULL,
  `grading_3` int(2) NOT NULL,
  `grading_4` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_grade`
--

INSERT INTO `student_grade` (`id`, `fk_student_id`, `fk_subject_id`, `fk_teacher_id`, `grading_1`, `grading_2`, `grading_3`, `grading_4`) VALUES
(1, 3, 1, 14, 1, 0, 0, 0),
(2, 45, 1, 14, 0, 0, 0, 0),
(3, 46, 1, 14, 0, 0, 0, 0),
(4, 50, 1, 14, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbi_answer`
--

CREATE TABLE `tbi_answer` (
  `id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL COMMENT 'userid',
  `teacher_id` int(11) NOT NULL,
  `tbi_question` int(11) NOT NULL,
  `user_answer` varchar(25) NOT NULL,
  `user_answer_val` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbi_answer`
--

INSERT INTO `tbi_answer` (`id`, `student_id`, `teacher_id`, `tbi_question`, `user_answer`, `user_answer_val`) VALUES
(1, 3, 14, 42, 'poor', 1),
(2, 3, 14, 41, 'poor', 1),
(3, 3, 14, 40, 'poor', 1),
(4, 3, 14, 39, 'poor', 1),
(5, 3, 14, 38, 'poor', 1),
(6, 3, 14, 37, 'good', 2),
(7, 3, 14, 36, 'good', 2),
(8, 3, 14, 35, 'good', 2),
(9, 3, 14, 34, 'good', 2),
(10, 3, 14, 33, 'good', 2),
(11, 3, 14, 32, 'good', 2),
(12, 3, 14, 31, 'good', 2),
(13, 3, 14, 30, 'very excelent', 5),
(14, 3, 14, 29, 'excelent', 4),
(15, 3, 14, 28, 'poor', 1),
(16, 3, 14, 27, 'very excelent', 5),
(17, 3, 14, 26, 'very excelent', 5),
(18, 3, 14, 25, 'good', 2),
(19, 3, 14, 24, 'very good', 3),
(20, 3, 14, 23, 'good', 2),
(21, 3, 14, 22, 'good', 2),
(22, 3, 14, 21, 'excelent', 4),
(23, 3, 14, 20, 'very good', 3),
(24, 3, 14, 19, 'excelent', 4),
(25, 3, 14, 18, 'good', 2),
(26, 3, 14, 17, 'very excelent', 5),
(27, 3, 14, 16, 'good', 2),
(28, 3, 14, 15, 'very good', 3),
(29, 3, 14, 14, 'excelent', 4),
(30, 3, 14, 13, 'excelent', 4),
(31, 3, 14, 12, 'very good', 3),
(32, 3, 14, 11, 'excelent', 4),
(33, 3, 14, 10, 'good', 2),
(34, 3, 14, 9, 'excelent', 4),
(35, 3, 14, 8, 'excelent', 4),
(36, 3, 14, 7, 'very good', 3),
(37, 3, 14, 6, 'good', 2),
(38, 3, 14, 5, 'excelent', 4),
(39, 3, 14, 4, 'very good', 3),
(40, 3, 14, 3, 'good', 2),
(41, 3, 47, 42, 'very excelent', 5),
(42, 3, 47, 41, 'very excelent', 5),
(43, 3, 47, 40, 'very excelent', 5),
(44, 3, 47, 39, 'very excelent', 5),
(45, 3, 47, 38, 'very excelent', 5),
(46, 3, 47, 37, 'good', 2),
(47, 3, 47, 36, 'good', 2),
(48, 3, 47, 35, 'good', 2),
(49, 3, 47, 34, 'good', 2),
(50, 3, 47, 33, 'good', 2),
(51, 3, 47, 32, 'good', 2),
(52, 3, 47, 31, 'good', 2),
(53, 3, 47, 30, 'very excelent', 5),
(54, 3, 47, 29, 'excelent', 4),
(55, 3, 47, 28, 'poor', 1),
(56, 3, 47, 27, 'very excelent', 5),
(57, 3, 47, 26, 'very excelent', 5),
(58, 3, 47, 25, 'good', 2),
(59, 3, 47, 24, 'very good', 3),
(60, 3, 47, 23, 'good', 2),
(61, 3, 47, 22, 'good', 2),
(62, 3, 47, 21, 'excelent', 4),
(63, 3, 47, 20, 'very good', 3),
(64, 3, 47, 19, 'excelent', 4),
(65, 3, 47, 18, 'good', 2),
(66, 3, 47, 17, 'very excelent', 5),
(67, 3, 47, 16, 'good', 2),
(68, 3, 47, 15, 'very good', 3),
(69, 3, 47, 14, 'excelent', 4),
(70, 3, 47, 13, 'excelent', 4),
(71, 3, 47, 12, 'very good', 3),
(72, 3, 47, 11, 'excelent', 4),
(73, 3, 47, 10, 'good', 2),
(74, 3, 47, 9, 'excelent', 4),
(75, 3, 47, 8, 'excelent', 4),
(76, 3, 47, 7, 'very good', 3),
(77, 3, 47, 6, 'good', 2),
(78, 3, 47, 5, 'excelent', 4),
(79, 3, 47, 4, 'very good', 3),
(80, 3, 47, 3, 'good', 2);

--
-- Triggers `tbi_answer`
--
DELIMITER $$
CREATE TRIGGER `load_ans_val` BEFORE INSERT ON `tbi_answer` FOR EACH ROW set new.user_answer_val = (
	case new.user_answer
    when 'poor' then 1
    when 'good' then 2
    when 'very good' then 3
    when 'excelent' then 4
    when 'very excelent' then 5
	else 0
    end    
)
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `tbi_answer_list`
--

CREATE TABLE `tbi_answer_list` (
  `id` int(11) NOT NULL,
  `answer_value` varchar(50) NOT NULL,
  `visible_to` enum('student','teacher') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbi_answer_list`
--

INSERT INTO `tbi_answer_list` (`id`, `answer_value`, `visible_to`) VALUES
(1, 'poor', 'student'),
(2, 'good', 'student'),
(3, 'very good', 'student'),
(4, 'excelent', 'student'),
(5, 'very excelent', 'student'),
(6, 'poor', 'teacher'),
(7, 'good', 'teacher'),
(8, 'very good', 'teacher'),
(9, 'excelent', 'teacher'),
(10, 'very excelent', 'teacher');

-- --------------------------------------------------------

--
-- Table structure for table `tbi_questions`
--

CREATE TABLE `tbi_questions` (
  `id` int(11) NOT NULL,
  `question` text NOT NULL,
  `questions_for` enum('teacher','student') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbi_questions`
--

INSERT INTO `tbi_questions` (`id`, `question`, `questions_for`) VALUES
(3, 'Gives Several examples of each concept.', 'student'),
(4, 'Uses concrete, everyday examples to explain concepts and principles.', 'student'),
(5, 'Fails to define new or unfamiliar terms.', 'student'),
(6, 'Repeats difficult ideas several times', 'student'),
(7, 'Stresses most important points by pausing, speaking slowly, raising voice etc.', 'student'),
(8, 'Answers students\' questions complicated ideas.', 'student'),
(9, 'Suggest ways of memorizing ideas.', 'student'),
(10, 'Speaks in dramatic and expressive way.', 'student'),
(11, 'Moves around the room while teaching', 'student'),
(12, 'Gestures with head, body, hands, and arm.', 'student'),
(13, 'Exhibits facial gestures or expressions.', 'student'),
(14, 'Avoids eye contact with students', 'student'),
(15, 'Tells jokes or humorous annecdotes', 'student'),
(16, 'Reads lectures verbatim from prepared notes or texts.', 'student'),
(17, 'Smiles or laughs while teaching interaction.', 'student'),
(18, 'Encourages students to ask questions or make comments.', 'student'),
(19, 'Praises students for good ideas.', 'student'),
(20, 'Asks questions of individual students.', 'student'),
(21, 'Asks questions of class as a whole.', 'student'),
(22, 'Incorporates students ideas to lecture.', 'student'),
(23, 'Use a variety of media and activities in class.', 'student'),
(24, 'Criticizes students when they make errors. ', 'student'),
(25, 'Gives preliminary overview of lecture.', 'student'),
(26, 'Puts outline of lecture on blackboard or overhead screen.', 'student'),
(27, 'Signals transition from one topic to the next.', 'student'),
(28, 'Explains how each topic fits into course as a whole.', 'student'),
(29, 'Periodically summarizes points previously made pacing.', 'student'),
(30, 'Digresses from major theme of lecture.', 'student'),
(31, 'Asks if students understand before proceeding to next topic.', 'student'),
(32, 'Sticks to the point in answering students questions.', 'student'),
(33, 'Advises students how to prepare for exams.', 'student'),
(34, 'Provides sample exam questions.', 'student'),
(35, 'Tells students exactly what is expected on tests, essays, and assignments.', 'student'),
(36, 'States objective of course as a whole.', 'student'),
(37, 'States objective of each class session.', 'student'),
(38, 'Reminds students of test dates and assignment deadlines.', 'student'),
(39, 'Speaks at appropriate volume.', 'student'),
(40, 'Speaks clearly.', 'student'),
(41, 'Addresses individual students by name.', 'student'),
(42, 'Talks to students before and after class.', 'student'),
(43, 'Talks to students before and after class.', 'teacher');

-- --------------------------------------------------------

--
-- Table structure for table `tbi_setting`
--

CREATE TABLE `tbi_setting` (
  `id` int(11) NOT NULL,
  `sy` varchar(9) NOT NULL,
  `teacher_tbi` tinyint(1) NOT NULL DEFAULT '0',
  `student_tbi` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbi_setting`
--

INSERT INTO `tbi_setting` (`id`, `sy`, `teacher_tbi`, `student_tbi`) VALUES
(18, '2015-2016', 1, 1),
(19, '2016-2017', 0, 0),
(20, '2017-2018', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbi_status`
--

CREATE TABLE `tbi_status` (
  `id` int(11) NOT NULL,
  `tbi_type` enum('student','teacher') DEFAULT 'student',
  `student_id` int(11) NOT NULL,
  `tbi_status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='status if tbi has been took';

--
-- Dumping data for table `tbi_status`
--

INSERT INTO `tbi_status` (`id`, `tbi_type`, `student_id`, `tbi_status`) VALUES
(3, 'student', 3, 1),
(4, 'student', 3, 1),
(5, 'student', 3, 1),
(6, 'student', 3, 1),
(7, 'student', 3, 1),
(8, 'student', 14, 1),
(9, 'student', 14, 1),
(10, 'student', 3, 1),
(11, 'student', 3, 1),
(12, 'student', 3, 1),
(13, 'student', 3, 1),
(14, 'student', 3, 1),
(15, 'student', 3, 1),
(16, 'student', 3, 1),
(17, 'student', 3, 1),
(18, 'student', 3, 1),
(19, 'student', 3, 1),
(20, 'student', 3, 1),
(21, 'student', 3, 1),
(22, 'student', 3, 1),
(23, 'student', 3, 1),
(24, 'student', 3, 1),
(25, 'student', 3, 1),
(26, 'student', 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_sections`
--

CREATE TABLE `tbl_sections` (
  `id` int(11) NOT NULL,
  `SectionTitle` varchar(350) NOT NULL,
  `Grade` varchar(350) NOT NULL,
  `isdelete` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_sections`
--

INSERT INTO `tbl_sections` (`id`, `SectionTitle`, `Grade`, `isdelete`) VALUES
(1, 'Ilang-Ilang', 'Grade 1', 1),
(2, 'Copernicus', 'Grade 7', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(5) NOT NULL,
  `lastname` varchar(200) NOT NULL,
  `firstname` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `levelID` int(5) NOT NULL,
  `dateadded` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isdelete` int(1) NOT NULL DEFAULT '1',
  `useryear` varchar(20) DEFAULT NULL,
  `usersection` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `lastname`, `firstname`, `email`, `username`, `password`, `levelID`, `dateadded`, `isdelete`, `useryear`, `usersection`) VALUES
(1, 'Principal', 'Principal', 'admin@gmail.com', 'principal', 'principal', 2, '2015-06-09 00:00:00', 1, '', ''),
(2, 'admin', 'it prof', 'admin@gmail.com', 'a-itprof', 'a-itprof', 3, '2015-09-28 09:24:20', 1, '', ''),
(3, 'student1', 'student-f', 'user@gmail.com', 'student1', 'student1', 1, '2015-06-09 18:15:39', 1, '1', '3'),
(5, 'bond', 'james', 'jb@gmail.com', 'superuser', 'superuser', 7, '2015-12-10 05:14:18', 1, '', ''),
(14, 'teacher', 'teacher-f', 'teacher@teacher.com', 'teacher', 'teacher', 4, '2016-01-23 03:59:44', 1, '', ''),
(16, 'faculty', 'it prof', 'x@x.comx', 'f-itprof', 'f-itprof', 6, '2016-01-23 03:59:44', 1, '', ''),
(35, 'registrar', 'registrar', 'registrar@registrar.ll', 'registrar', 'registrar', 5, '2016-01-23 18:41:42', 1, '', ''),
(45, 'q', 'q', 'q@e.xx', 'q', 's', 1, '2016-01-24 05:04:29', 1, '5', '10'),
(46, 'mark', 'mark', 'q@e.xx', 'mark', 'mark', 1, '2016-01-24 05:14:25', 1, '1', '3'),
(47, 'teacher2', 'teacher2', 'teacher@teacher.com', 'teacher2', 'teacher2', 4, '2016-01-23 03:59:44', 1, '', ''),
(49, 'q1', 'q1', 'q1@qq.xx', 'q1', '!', 5, '2016-02-17 03:17:11', 1, 'null', 'null'),
(50, 'zz', 'zz', 'a@aa.aa', 'zz', '22junejie', 1, '2016-03-14 17:44:37', 1, '1', '3');

-- --------------------------------------------------------

--
-- Table structure for table `user_grade`
--

CREATE TABLE `user_grade` (
  `id` int(11) NOT NULL,
  `label` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_grade`
--

INSERT INTO `user_grade` (`id`, `label`) VALUES
(1, 'Grade 7'),
(2, 'Grade 8'),
(4, 'Grade 9'),
(5, 'Grade 10'),
(6, 'Grade 11'),
(7, 'Grade 12');

-- --------------------------------------------------------

--
-- Table structure for table `user_section`
--

CREATE TABLE `user_section` (
  `id` int(11) NOT NULL,
  `fk_grade_id` int(11) NOT NULL,
  `label` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_section`
--

INSERT INTO `user_section` (`id`, `fk_grade_id`, `label`) VALUES
(3, 1, 'Archimedes'),
(4, 1, 'Euler'),
(5, 1, 'Euclid'),
(6, 1, 'Galileo'),
(7, 2, 'Newton'),
(8, 2, 'Roentgen'),
(9, 2, 'Avogadro'),
(10, 2, 'Dalton'),
(13, 4, 'Curie'),
(14, 4, 'Fleming'),
(15, 4, 'Aristotle'),
(16, 4, 'Einstein'),
(17, 5, 'Edison'),
(18, 5, 'Mendeleev'),
(19, 5, 'Michaelson'),
(20, 5, 'Villar'),
(21, 6, 'Faraday'),
(22, 6, 'Lavoisier'),
(23, 6, 'Rockerfeller'),
(24, 6, 'Tesla'),
(25, 7, 'Darwin'),
(26, 7, 'Hawking'),
(27, 7, 'Maxwell'),
(28, 7, 'Pascal');

-- --------------------------------------------------------

--
-- Table structure for table `user_subject`
--

CREATE TABLE `user_subject` (
  `id` int(11) NOT NULL,
  `fk_grade_id` int(11) NOT NULL,
  `label` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_subject`
--

INSERT INTO `user_subject` (`id`, `fk_grade_id`, `label`) VALUES
(1, 1, 'Filipino 1'),
(2, 1, 'Araling Panlipunan 1'),
(5, 1, 'Values Education 1'),
(6, 1, 'Science & Technology 1'),
(7, 2, 'Filipino 2'),
(8, 2, 'Araling Panlipunan 2'),
(9, 2, 'Values Education 2'),
(10, 2, 'Science & Technology 2'),
(11, 4, 'Filipino 3'),
(12, 4, 'Values Education 3'),
(13, 4, 'Economics'),
(14, 4, 'Araling Panlipunan 3'),
(15, 5, 'English 4'),
(16, 5, 'Calculus 1'),
(17, 5, 'Statistics 3'),
(18, 5, 'Analytic Geometry'),
(19, 6, 'Komunikasyon at pananaliksik sa wika at kulturang pilipino'),
(20, 6, 'Pagbasa at Pagsusuri'),
(21, 6, '21st Century Literature of the Philippines'),
(22, 6, 'Contemporary Philippine Arts'),
(23, 7, 'Understanding Culture Society and Politics'),
(24, 7, 'Earth Science'),
(25, 7, 'Personal Development'),
(26, 7, 'Physical Education'),
(27, 1, 'Algebra'),
(28, 1, 'Geometry 1'),
(29, 1, 'P.E 1'),
(30, 1, 'English 1'),
(31, 2, 'Biology'),
(32, 2, 'Elementary Algebra'),
(33, 2, 'Geometry 2'),
(34, 2, 'Statistics 1'),
(35, 2, 'P.E 2'),
(36, 2, 'English 2'),
(37, 4, 'Science & Technology 3'),
(38, 4, 'Chemistry'),
(39, 4, 'Advanced Algebra'),
(40, 4, 'Geometry 3'),
(41, 4, 'Statistics 2'),
(42, 4, 'P.E. 3'),
(43, 4, 'English 3'),
(44, 4, 'Trigonometry'),
(45, 5, 'Filipino 4'),
(46, 5, 'Araling Panlipunan 4'),
(47, 5, 'Values Education 4'),
(48, 5, 'Trigonometry 2'),
(49, 5, 'Science & Technology 4'),
(50, 5, 'Chemistry 2'),
(51, 5, 'Physics'),
(52, 5, 'Advanced Algebra 2'),
(53, 5, 'P.E. 4'),
(54, 6, 'Reading And Writing'),
(55, 6, 'Oral Communication'),
(56, 6, 'Media and Information Literacy'),
(57, 7, 'Physical Science'),
(58, 7, 'Philosophy of the Human Person'),
(59, 7, 'General Math'),
(60, 7, 'Statistics And Probability'),
(61, 7, 'Disaster Readiness and Risk Reduction');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `account_type`
--
ALTER TABLE `account_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `announce_to_year` (`activity_to_year`),
  ADD KEY `announce_to_section` (`activity_to_section`),
  ADD KEY `announce_by` (`activity_by`);

--
-- Indexes for table `activities_answer`
--
ALTER TABLE `activities_answer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_activity` (`fk_activity`),
  ADD KEY `fk_studentid` (`fk_studentid`);

--
-- Indexes for table `announcement`
--
ALTER TABLE `announcement`
  ADD PRIMARY KEY (`id`),
  ADD KEY `announce_to_year` (`announce_to_year`),
  ADD KEY `announce_to_section` (`announce_to_section`),
  ADD KEY `announce_by` (`announce_by`);

--
-- Indexes for table `calendar`
--
ALTER TABLE `calendar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `announce_to_year` (`to_year`),
  ADD KEY `announce_to_section` (`to_section`),
  ADD KEY `announce_by` (`saved_by`);

--
-- Indexes for table `grade_setting`
--
ALTER TABLE `grade_setting`
  ADD PRIMARY KEY (`id`),
  ADD KEY `teacher_id` (`teacher_id`);

--
-- Indexes for table `lectures`
--
ALTER TABLE `lectures`
  ADD PRIMARY KEY (`id`),
  ADD KEY `yearid` (`yearid`),
  ADD KEY `sectionid` (`sectionid`),
  ADD KEY `subjectid` (`subjectid`),
  ADD KEY `added_by` (`added_by`);

--
-- Indexes for table `schedule`
--
ALTER TABLE `schedule`
  ADD PRIMARY KEY (`id`),
  ADD KEY `teacher_id` (`teacher_id`),
  ADD KEY `grade_id` (`grade_id`),
  ADD KEY `section_id` (`section_id`),
  ADD KEY `subject_it` (`subject_id`);

--
-- Indexes for table `setting`
--
ALTER TABLE `setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student_grade`
--
ALTER TABLE `student_grade`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_student_id` (`fk_student_id`),
  ADD KEY `fk_subject_id` (`fk_subject_id`),
  ADD KEY `fk_teacher_id` (`fk_teacher_id`);

--
-- Indexes for table `tbi_answer`
--
ALTER TABLE `tbi_answer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tbi_question` (`tbi_question`),
  ADD KEY `student_id` (`student_id`),
  ADD KEY `teacher_id` (`teacher_id`);

--
-- Indexes for table `tbi_answer_list`
--
ALTER TABLE `tbi_answer_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbi_questions`
--
ALTER TABLE `tbi_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbi_setting`
--
ALTER TABLE `tbi_setting`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sy` (`sy`);

--
-- Indexes for table `tbi_status`
--
ALTER TABLE `tbi_status`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student_id` (`student_id`);

--
-- Indexes for table `tbl_sections`
--
ALTER TABLE `tbl_sections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `levelID` (`levelID`);

--
-- Indexes for table `user_grade`
--
ALTER TABLE `user_grade`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_section`
--
ALTER TABLE `user_section`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_grade_id` (`fk_grade_id`);

--
-- Indexes for table `user_subject`
--
ALTER TABLE `user_subject`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_grade_id` (`fk_grade_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `account_type`
--
ALTER TABLE `account_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `activities_answer`
--
ALTER TABLE `activities_answer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `announcement`
--
ALTER TABLE `announcement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `calendar`
--
ALTER TABLE `calendar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `grade_setting`
--
ALTER TABLE `grade_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=303;
--
-- AUTO_INCREMENT for table `lectures`
--
ALTER TABLE `lectures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `schedule`
--
ALTER TABLE `schedule`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;
--
-- AUTO_INCREMENT for table `setting`
--
ALTER TABLE `setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `student_grade`
--
ALTER TABLE `student_grade`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tbi_answer`
--
ALTER TABLE `tbi_answer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;
--
-- AUTO_INCREMENT for table `tbi_answer_list`
--
ALTER TABLE `tbi_answer_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tbi_questions`
--
ALTER TABLE `tbi_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `tbi_setting`
--
ALTER TABLE `tbi_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `tbi_status`
--
ALTER TABLE `tbi_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `tbl_sections`
--
ALTER TABLE `tbl_sections`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
--
-- AUTO_INCREMENT for table `user_grade`
--
ALTER TABLE `user_grade`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `user_section`
--
ALTER TABLE `user_section`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `user_subject`
--
ALTER TABLE `user_subject`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `activities`
--
ALTER TABLE `activities`
  ADD CONSTRAINT `activities_ibfk_1` FOREIGN KEY (`activity_to_year`) REFERENCES `user_grade` (`id`),
  ADD CONSTRAINT `activities_ibfk_2` FOREIGN KEY (`activity_to_section`) REFERENCES `user_section` (`id`),
  ADD CONSTRAINT `activities_ibfk_3` FOREIGN KEY (`activity_by`) REFERENCES `tbl_user` (`id`);

--
-- Constraints for table `activities_answer`
--
ALTER TABLE `activities_answer`
  ADD CONSTRAINT `activities_answer_ibfk_1` FOREIGN KEY (`fk_activity`) REFERENCES `activities` (`id`),
  ADD CONSTRAINT `activities_answer_ibfk_2` FOREIGN KEY (`fk_studentid`) REFERENCES `tbl_user` (`id`);

--
-- Constraints for table `announcement`
--
ALTER TABLE `announcement`
  ADD CONSTRAINT `announcement_ibfk_1` FOREIGN KEY (`announce_to_year`) REFERENCES `user_grade` (`id`),
  ADD CONSTRAINT `announcement_ibfk_2` FOREIGN KEY (`announce_to_section`) REFERENCES `user_section` (`id`),
  ADD CONSTRAINT `announcement_ibfk_3` FOREIGN KEY (`announce_by`) REFERENCES `tbl_user` (`id`);

--
-- Constraints for table `grade_setting`
--
ALTER TABLE `grade_setting`
  ADD CONSTRAINT `grade_setting_ibfk_1` FOREIGN KEY (`teacher_id`) REFERENCES `tbl_user` (`id`);

--
-- Constraints for table `lectures`
--
ALTER TABLE `lectures`
  ADD CONSTRAINT `lectures_ibfk_1` FOREIGN KEY (`yearid`) REFERENCES `user_grade` (`id`),
  ADD CONSTRAINT `lectures_ibfk_2` FOREIGN KEY (`sectionid`) REFERENCES `user_section` (`id`),
  ADD CONSTRAINT `lectures_ibfk_3` FOREIGN KEY (`subjectid`) REFERENCES `user_subject` (`id`),
  ADD CONSTRAINT `lectures_ibfk_4` FOREIGN KEY (`added_by`) REFERENCES `tbl_user` (`id`);

--
-- Constraints for table `schedule`
--
ALTER TABLE `schedule`
  ADD CONSTRAINT `schedule_ibfk_1` FOREIGN KEY (`teacher_id`) REFERENCES `tbl_user` (`id`),
  ADD CONSTRAINT `schedule_ibfk_2` FOREIGN KEY (`grade_id`) REFERENCES `user_grade` (`id`),
  ADD CONSTRAINT `schedule_ibfk_3` FOREIGN KEY (`section_id`) REFERENCES `user_section` (`id`),
  ADD CONSTRAINT `schedule_ibfk_4` FOREIGN KEY (`subject_id`) REFERENCES `user_subject` (`id`);

--
-- Constraints for table `student_grade`
--
ALTER TABLE `student_grade`
  ADD CONSTRAINT `student_grade_ibfk_1` FOREIGN KEY (`fk_student_id`) REFERENCES `tbl_user` (`id`),
  ADD CONSTRAINT `student_grade_ibfk_2` FOREIGN KEY (`fk_subject_id`) REFERENCES `user_subject` (`id`),
  ADD CONSTRAINT `student_grade_ibfk_3` FOREIGN KEY (`fk_teacher_id`) REFERENCES `tbl_user` (`id`);

--
-- Constraints for table `tbi_answer`
--
ALTER TABLE `tbi_answer`
  ADD CONSTRAINT `tbi_answer_ibfk_1` FOREIGN KEY (`tbi_question`) REFERENCES `tbi_questions` (`id`),
  ADD CONSTRAINT `tbi_answer_ibfk_2` FOREIGN KEY (`student_id`) REFERENCES `tbl_user` (`id`),
  ADD CONSTRAINT `tbi_answer_ibfk_3` FOREIGN KEY (`teacher_id`) REFERENCES `tbl_user` (`id`);

--
-- Constraints for table `tbi_status`
--
ALTER TABLE `tbi_status`
  ADD CONSTRAINT `tbi_status_ibfk_1` FOREIGN KEY (`student_id`) REFERENCES `tbl_user` (`id`);

--
-- Constraints for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD CONSTRAINT `tbl_user_ibfk_1` FOREIGN KEY (`levelID`) REFERENCES `account_type` (`id`);

--
-- Constraints for table `user_section`
--
ALTER TABLE `user_section`
  ADD CONSTRAINT `user_section_ibfk_1` FOREIGN KEY (`fk_grade_id`) REFERENCES `user_grade` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user_subject`
--
ALTER TABLE `user_subject`
  ADD CONSTRAINT `user_subject_ibfk_1` FOREIGN KEY (`fk_grade_id`) REFERENCES `user_grade` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

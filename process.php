<?php 
require_once('Connections/conn.php'); 
$connection = db_connect();

$txtUsername	 = mysqli_real_escape_string($connection,$_POST['txtUsername']);
$txtPassword	 = mysqli_real_escape_string($connection,$_POST['txtPassword']);

$sql="
	select * 
	from tbl_user 
	where username='$txtUsername' 
	and password='$txtPassword' 
	and isdelete = 1";


if(isset($_POST['btnLogin'])){
	$query = mysqli_query($connection,$sql);
	$row = mysqli_fetch_assoc($query);	
	$counter = mysqli_num_rows($query);

	$id = $row['levelID'];
	//Account_type
	$account_type="
		select * 
		from account_type 
		where id = '$id' ";
	$accnt_type_query = mysqli_query($connection,$account_type);
	$accnt_type_row = mysqli_fetch_assoc($accnt_type_query);
	$_SESSION['user_type'] = $accnt_type_row['user_type'];

	//User Grade
	$gradeid = $row['useryear'];
	$user_grade = "
		select * 
		from user_grade 
		where id = '$gradeid' ";
	$user_grade_query = mysqli_query($connection,$user_grade);
	$user_grade_row = mysqli_fetch_assoc($user_grade_query);
	$_SESSION['user_gradelabel'] = $user_grade_row['label'];

	//User Section
	$sectionid = $row['usersection'];
	$user_section = "
		select * 
		from user_section 
		where id = '$sectionid' ";
	$user_section_query = mysqli_query($connection,$user_section);
	$user_section_row = mysqli_fetch_assoc($user_section_query);
	$_SESSION['user_sectionlabel'] = $user_section_row['label'];


	if($counter==1){
		$_SESSION['id']  =$row['id'];
		$_SESSION['useryear']  =$row['useryear'];
		$_SESSION['usersection']  =$row['usersection'];

		//STUDENT
		if($row['levelID'] == 1){
			$_SESSION['levelID'] =$row['levelID'];
			$_SESSION['userID'] =$row['id'];
			$_SESSION['fullname'] =$row['firstname']." ".$row['lastname'];
			$_SESSION['restrictLogin']=1;
			 header("location:student.php");

		//Admin IT
		}elseif($row['levelID'] == 2){
			$_SESSION['levelID'] =$row['levelID'];
			$_SESSION['userID'] =$row['id'];
			$_SESSION['restrictLogin']=1;
			$_SESSION['fullname'] =$row['firstname']." ".$row['lastname'];

			header("location:main.php"); 
		}elseif($row['levelID'] == 3){
			$_SESSION['levelID'] =$row['levelID'];
			$_SESSION['userID'] =$row['id'];
			$_SESSION['restrictLogin']=1;
			$_SESSION['fullname'] =$row['firstname']." ".$row['lastname'];

			header("location:main.php"); 
		// }
		// elseif($row['levelID'] == 4){
		// 	$_SESSION['levelID'] =$row['levelID'];
		// 	$_SESSION['userID'] =$row['id'];
		// 	$_SESSION['restrictLogin']=1;
		// 	$_SESSION['fullname'] =$row['firstname']." ".$row['lastname'];

		// 	header("location:main.php"); 

		//Super user
		}elseif($row['levelID'] == 7){
			$_SESSION['levelID'] =$row['levelID'];
			$_SESSION['userID'] =$row['id'];
			$_SESSION['restrictLogin']=1;
			$_SESSION['fullname'] =$row['firstname']." ".$row['lastname'];

			header("location:users.php"); 

		//Registrar
		}elseif($row['levelID'] == 5){
			$_SESSION['levelID'] =$row['levelID'];
			$_SESSION['userID'] =$row['id'];
			$_SESSION['restrictLogin']=1;
			$_SESSION['fullname'] =$row['firstname']." ".$row['lastname'];

			header("location:registrar.php"); 
		
		//IT PROF
		}elseif( $row['levelID'] == 6 or $row['levelID'] == 4){

			$_SESSION['levelID'] =$row['levelID'];
			$_SESSION['userID'] =$row['id'];
			$_SESSION['restrictLogin']=1;
			$_SESSION['fullname'] =$row['firstname']." ".$row['lastname'];
			header("location:it-prof.php"); 
		
		}
	}else{
		header("location:error.php");
	}
	mysqli_close($connection);	
}

//addUser
if(isset($_POST['btnAddUser'])){
	$sql = "insert into tbl_user (`lastname`,`firstname`,`email`,`username`,`password`,`levelID`,`dateadded`) values ('$_POST[txtln]', '$_POST[txtfn]','$_POST[txtemail]', '$_POST[txtun]', '$_POST[txtpw]',2, NOW());";
	mysqli_query($connection,$sql);
	mysqli_close($connection);
	header("location:index.php");
}


//update section

if(isset($_POST['btnEditSection']))
{
	echo $sql="update tbl_sections set itemtype='$_POST[secName]', categoryID='$_POST[gradelvl]' where section_id='$_POST[secID]';";
	mysqli_query($connection,$sql);
}

//delete section
if(isset($_POST['btnDeleteSection']))
{
	$sql = "update tbl_sections set `isdelete`=0 where `section_id`='$_POST[sID]';";
	mysqli_query($connection,$sql);
	
	mysqli_close($connection);
	header("location:encode-grade.php");
}

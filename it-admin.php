<?php 
	require_once('Connections/conn.php');
	include('inc_security.php');
	include('app/proc/permission.php');
	include_once('app/proc/User.php');
	require_once('app/lib/js-lib.php');

	$connection = db_connect();
	if (
		$_SESSION['levelID']=='2' or 
		$_SESSION['levelID']=='3'
		) {
		$where = " group_user != 'superuser' ";
	} else {
		$where = '0';
	}
	
	$userlist = getUserList($connection,$where);
	$grade = getGradeLevel($connection);
	$section = getSections($connection);
	$announcementList = getAnnouncement($connection);
	$allUsers = getAllUsers($connection);
	$getTBISetting = getTBISetting($connection);
	$getGradeSetting = getGradeSetting($connection);
	$lectureList = getlectureList($connection);
	$activityList = getActivity($connection);
	$tbiSetting = getUserTBISetting($connection,'teacher');
	$questions = getQuestions($connection,'teacher');
	$tbiAnswerList = gettbiAnswerList($connection,'teacher');
	$teachers = getTeachers($connection);
	$isSaveGrade = getIsSaveGrade($connection)[0]['isenable'];
	$students = getStudents($connection,'true',10);
	$getTeachersForTBIListT = getTeachersForTBIListT($connection);
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<title><?php include('assets/includes/inc_title.php');?></title>

   	<!-- Bootstrap CSS -->
  	<link rel="stylesheet" href="assets/css/bootstrap.css">
  	<link rel="stylesheet" href="assets/css/bootstrap-switch.css">
  	<link rel="stylesheet" href="assets/css/bootstrap-toggle.min.css">
  	<!-- Bootstrap JS -->
	<script src="assets/js/jquery.js"></script>
	<script src="assets/js/bootstrap-toggle.min.js"></script>
	<script src="assets/js/bootstrap-switch.js"></script>
	<script src="assets/js/bootstrap.js"></script>  	
	<noscript>
		<style>
			div#tab2 {
			    padding-top: 10px;
			}
			.da-thumbs li a div {
				top: 0px;
				left: -100%;
				-webkit-transition: all 0.3s ease;
				-moz-transition: all 0.3s ease-in-out;
				-o-transition: all 0.3s ease-in-out;
				-ms-transition: all 0.3s ease-in-out;
				transition: all 0.3s ease-in-out;
			}
			.da-thumbs li a:hover div{
				left: 0px;
			}
		</style>
	</noscript>	

  	<link rel="stylesheet" href="assets/css/DT_bootstrap.css">
  	<link rel="stylesheet" href="assets/css/print.css">
  	<link rel="stylesheet" href="assets/css/tab.css">

	<!-- FAVICON ICON-->
    <link rel="shortcut icon" href="images/favicon.ico"/>

  	<link rel="stylesheet" href="assets/css/tbi-setting.css">

  	<!-- upload plugin -->
	<link href="assets/js/plugins/bootstrap-fileinput/css/fileinput.min.css" 
	media="all" rel="stylesheet" type="text/css" />
	<script src="assets/js/plugins/bootstrap-fileinput/js/plugins/canvas-to-blob.min.js" type="text/javascript"></script>
	<script src="assets/js/plugins/bootstrap-fileinput/js/fileinput.min.js"></script>
	<script src="assets/js/plugins/bootstrap-fileinput/js/fileinput_locale_LANG.js"></script>

	<script>
		$(document).ready(function(){
			$('#product-table a:first').tab('show');
		});
	</script>
	<script type="text/javascript" charset="utf-8" language="javascript" src="assets/js/jquery.dataTables.js"></script>
    <script type="text/javascript" charset="utf-8" language="javascript" src="assets/js/DT_bootstrap.js"></script>

    <!-- plugin dualist -->
	<script src="assets/js/plugins/bootstrap-duallistbox-3.0.5/dist/jquery.bootstrap-duallistbox.min.js"></script>
	<link rel="stylesheet" type="text/css" 
	href="assets/js/plugins/bootstrap-duallistbox-3.0.5/dist/bootstrap-duallistbox.min.css">

</head>
<body>
	<div class="alert alert-success hidden">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<strong>Success!</strong>
	</div>
	<div class="gridContainer clear">
		<?php include("assets/includes/inc_navigation.php");?>
		<div class="container">
			<div class="col-lg-12"> 
			    <div class="bs-component">
			        <ul class="nav nav-tabs responsive">
			            <li class="active">
			                <a data-toggle="tab" href="#tab1">Accounts</a>
			            </li>
			            <li>    
			                <a data-toggle="tab" href="#grade-tab">Grades</a>
			            </li>
			            <li>
			                <a data-toggle="tab" href="#tab4">Announcement</a>
			            </li>
			            <?php echo permissions('lectures-tab');?>
			            <li>
			                <a data-toggle="tab" href="#activity-tab">Activity</a>
			            </li>
			            <?php echo permissions('tbisettingtab');?>
			            <li>
			                <a data-toggle="tab" href="#tbi-tab">OTBI</a>
			            </li>
			         </ul>
			        <div class="tab-content">
			            <div id="tab1" class="tab-pane fade active in">
							<?php  
							include('app/form/account.php');
							?>
			            </div>
			            <div id="grade-tab" class="tab-pane fade active">
							<?php
								include('app/form/grade-filter.php');
								include('app/form/grade-setting.php');
								include('app/form/grade-table.php');
							?>
			            </div>
				        <div id="tab4" class="tab-pane fade active">
				        	<?php include('app/form/announcement-tab.php');?>
				        </div>
				        <div id="tab6" class="tab-pane fade active">
				        	<?php include 'app/form/tbi-setting.php';?>
				        </div>
			             <div id="lectures-tab" class="tab-pane fade active">
				        	<?php include('app/form/lectures-tab.php');?> 
			            </div>
			            <div id="activity-tab" class="tab-pane fade active">
				        	<?php include('app/form/activity-tab.php');?>
				        	<?php include('app/modal/delete-activity.php');?>
				        </div>
				        <div id="tbi-tab" class="tab-pane fade active">
				        	<?php 
				        	if ( $tbiSetting ){
				        		include('app/form/tbi-for-teacher-tab.php');
				        	}else{
				        		include('app/form/info.php');
				        	}
				        	?>
				        </div>
			        </div>
			    </div>
			</div>
		</div>
	<!-- load js -->
<?php
	loadJS($_SESSION['levelID']);
?>
</body>
</html>

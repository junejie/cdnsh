<table cellpadding="0" cellspacing="0" border="0" 
class="table  table-bordered" id="example">
    
    <div class="alert alert-info">
    <?php echo permissions('useradd');?>
	<?php include('app/modal/add-user.php'); ?>					           
    </div>

    <thead>
    	<tr>
			<th>First Name</th>
			<th>Last Name</th>
			<th>Email</th>
			<th>User Name</th>
			<th>User Type</th>
			<th>Actions</th>
		</tr>
    </thead>
    <tbody>
		<?php
		foreach ($userlist as $key => $row) {
		?>
		<tr>
			<td><?php echo $row['firstname']; ?></td>
			<td><?php echo $row['lastname']; ?></td>
			<td><?php echo $row['email']; ?></td>
			<td><?php echo $row['username']; ?></td>
			<td><?php echo $row['user_type']; ?></td>
			<td>
				<!-- edit user -->

				<button type="button" <?php echo permissions('useredit'); ?>
				class="<?php echo permissions('useredit'); ?> btn btn-info  btn-xs" 
				data-toggle="modal" 
				data-target="#editModal<?=$row['id'];?>">
				<span class="glyphicon glyphicon-edit">
				</span> Edit User</button>	 
				<?php include('app/modal/edit-user.php');?>

				<!-- delete user -->
				<button type="button" <?php echo permissions('userdelete'); ?>
				class="<?php echo permissions('userdelete'); ?> btn btn-danger btn-xs" 
				data-toggle="modal" 
				data-target="#deleteModal<?=$row['id'];?>">
				<span class="glyphicon glyphicon-trash">
				</span> Delete User</button>	 
				<?php include('app/modal/delete-user.php');?>
			</td>
		</tr>
			<?php } ?>
    </tbody>
</table>
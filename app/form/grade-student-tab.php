<table id="tblGrades" 
class="table table-condensed table-bordered table-striped volumes">
    <tbody>
        <tr>
            <th></th>
            <th>1st</th>
            <th>2nd</th>
            <th>3rd</th>
            <th>4th</th>
        </tr>
        <?php            
            $isEditable = ($_SESSION['levelID']=='1'? '':'contenteditable');
        	foreach ($students as $key => $value) {
        ?>
        <tr>
            <th>
            	<?php 
            	$label = $value['firstname']. ' '. $value['lastname'];
            	echo $label;
            	?>
            	<input name="g0_" id="g0_" type="hidden" value="<?=$value['id'];?>">
            </th>
            <td><span id="g1_" name="g1_"  <?=$isEditable;?> ><?=$value['grading_1'];?></span></td>
            <td><span id="g2_" name="g2_"  <?=$isEditable;?> ><?=$value['grading_2'];?></span></td>
            <td><span id="g3_" name="g3_"  <?=$isEditable;?> ><?=$value['grading_3'];?></span></td>
            <td><span id="g4_" name="g4_"  <?=$isEditable;?> ><?=$value['grading_4'];?></span></td>
        </tr>
        <?php 
        	}
        ?>
    </tbody>
</table>
<table cellpadding="0" cellspacing="0" border="0" 
class="table  table-bordered" id="example">
	<?php if( $_SESSION['levelID']  != 1){?>
    <div class="alert alert-info">
        <button type="button" class="btn btn-info" data-toggle="modal" 
        data-target="#addActivity">Add Activity</button>	 
	    <!-- Modal add activity -->
		<?php include('app/modal/add-activity.php');?>
    </div>
    <?php }else{ ?>
	<div style="height:20px;"></div>
    <?php	} ?>
    <thead>
    	<tr>
            <th align="center">Assigned To</th>
			<th align="center">Date</th>
			<th align="center">Actions</th>
		</tr>
    </thead>
    <tbody>
		<?php
		foreach ($activityList as $key => $row) {
		?>
			<tr>
				<td data-toggle="modal" 
                data-target="#act_<?=$row['id']?>" style="cursor: pointer;">
                <?php echo $row['gradename'] .' - '. $row['sectionname']; ?>
                </td>
                <td data-toggle="modal" 
                data-target="#act_<?=$row['id']?>" style="cursor: pointer;">
                <?php echo $row['activity_date']; ?>
                </td>
                <td>
                    <button type="button" class="btn btn-danger btn-xs" data-toggle="modal" 
                        data-target="#deleteActivity<?=$row['id']?>">
                    <span class="glyphicon glyphicon-trash">
                    </span> Delete</button>
                    <button onclick="viewActivitiesFromStudent(<?=$row['id']?>)" type="button" 
                    class="btn btn-info btn-xs"
                        data-toggle="modal" data-target="#viewActivitiesFromStudent<?=$row['id']?>">
                        <span class="glyphicon glyphicon-th-list" ></span>
                    </button>
                </td>
			</tr>

            <!-- delete activity -->
            <div id="deleteActivity<?=$row['id'];?>" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <form role="form" id="deleteactivity">
                            <div class="modal-header">
                                <h4 class="modal-title">Delete activity</h4>
                            </div>
                            <div class="modal-body alert alert-danger">
                                <div class="form-group">
                                    <p>Are you sure you want to delete this activity?</p>
                                    <input type="hidden" class="form-control" id="deleteactivity" 
                                    value="<?=$row['id']; ?>">
                                </div>
                            </div>  
                            <div class="modal-footer">
                                <button type="button" 
                                onclick="deleteActivity('<?=$row['id'];?>')" class="btn btn-primary">Submit
                                </button>
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <!-- activity modal view student list -->
            <div id="viewActivitiesFromStudent<?=$row['id']?>" class="modal fade" role="dialog">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <span>
                                List of Submitted Activities
                            </span>
                        </div>
                        <div class="modal-body">
                            <div id="divtable-<?=$row['id']?>"></div>
                           <!--  -->
                        </div>
                    </div>
                </div>
            </div>

            <!-- activity modal download/update -->
            <div id="act_<?=$row['id']?>" class="modal fade" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <span>
                                Activity Title: <?=$row['content']?>
                            </span>
                        </div>
                        <form role="form" name="frmUpdateactivity">
                            <div class="modal-body">
                                <div class="form-group">
                                    <a download="<?=$row['docname']?>" 
                                    href="upload/activity/<?=$row['filename']?>">
                                    <button type="button" class="btn btn-info btn-xs">
                                        <span class="glyphicon glyphicon-download">
                                        </span> 
                                    Download</button>
                                    </a>
                                    <span>
                                        <?=$row['docname']?>
                                    </span>
                                </div>
                                <input type="hidden" value="<?=$row['id']?>">
                                <div class="form-group">
                                    <label for="activitystatus">Status</label>
                                    <select id="activitystatus" class="form-control" >
                                        <option value="open">Open</option>
                                        <option value="close">Close</option>
                                    </select>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <button type="button" class="btn btn-default" 
                                data-dismiss="modal">Close</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
		<?php } ?>
    </tbody>
</table>
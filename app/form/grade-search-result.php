<?php 
	if (count($students) == 0) {
		?>
			<form role="form" id="addgrade">
				<h2>No Students Enrolled</h2>
			</form>
		<?php
	}else{
		?>	
<form role="form" id="addgrade">
	<table id="tblGrades" class="table table-condensed table-bordered table-striped volumes">
	    <tbody>
	        <tr>
	            <th></th>
	            <th>1st</th>
	            <th>2nd</th>
	            <th>3rd</th>
	            <th>4th</th>
	        </tr>
	        <?php
	        	foreach ($students as $key => $value) {
	        ?>
	        <tr>
	            <th>
	            	<?php 
	            	$label = $value['firstname']. ' '. $value['lastname'];
	            	echo $label;
	            	?>
	            	<input name="g0_" id="g0_" type="hidden" value="<?=$value['id'];?>">
	            </th>
	            <td><span class="editablecontent_cl" id="g1_" name="g1_" 
	            	<?=($isSaveGrade=='1'? ' contenteditable ':'')  ?>
	            ><?=$value['grading_1'];?></span></td>
	            <td><span class="editablecontent_cl" id="g2_" name="g2_" 
	            	<?=($isSaveGrade=='1'? ' contenteditable ':'')  ?>
	            ><?=$value['grading_2'];?></span></td>
	            <td><span class="editablecontent_cl" id="g3_" name="g3_" 
	            	<?=($isSaveGrade=='1'? ' contenteditable ':'')  ?>
	            ><?=$value['grading_3'];?></span></td>
	            <td><span class="editablecontent_cl" id="g4_" name="g4_" 
	            	<?=($isSaveGrade=='1'? ' contenteditable ':'')  ?>
	            ><?=$value['grading_4'];?></span></td>
	        </tr>
	        <?php 
	        	}
	        ?>
	    </tbody>
	</table>
	<div class="contrainer-fluid">
		<div class="row">
			<div class="col-md-11"></div>
			<div class="col-md-1">
				<button type="button" id="btnAddGrade" 
				<?=($isSaveGrade=='0'? ' disabled="disabled" ':'')  ?>
				class="btn btn-primary <?=($isSaveGrade=='0'? 'disabled':'')  ?>">
				Save</button>
			</div>
		</div>
	</div>	
</form>


<?php
	}
?>

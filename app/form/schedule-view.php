<table class="table table-condensed table-bordered table-striped volumes">
	<thead>
		<tr>
			<td>Grade</td>
			<td>Section</td>
			<td>Subject</td>
			<td>Start Time</td>
			<td>End Time</td>
			
		</tr>
	</thead>
	<tbody>
		<?php  
		$data = '';
		foreach ($sched as $key => $value) {
			$data .= '<tr>';
			$data .= '<td>'.$value['gradename'].'</td>';
			$data .= '<td>'.$value['sectionname'].'</td>';
			$data .= '<td>'.$value['subjectname'].'</td>';
			$data .= '<td>'.$value['start_time'].'</td>';
			$data .= '<td>'.$value['end_time'].'</td>';
			$data .= '</tr>';
			echo $data;
		}
		?>
	</tbody>

</table>
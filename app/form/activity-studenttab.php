<table cellpadding="0" cellspacing="0" border="0" class="table table-bordered" 
	id="example">
	<?php if( $_SESSION['levelID']  != 1){?>
    <div class="alert alert-info">
        <button type="button" class="btn btn-info" data-toggle="modal" 
        data-target="#addActivity">Add Activity</button>	 
	    <!-- Modal add activity -->
		<?php include('app/modal/add-activity.php');?>
    </div>
    <?php }else{ ?>
	<div style="height:20px;"></div>
    <?php	} ?>
    <thead>
    	<tr>
			<th align="center">Date</th>
            <th>Assigned By</th>
            <th>Title</th>
			<th>File</th>
            <th>Action</th>
            <th>Status</th>
		</tr>
    </thead>
    <tbody>
		<?php
		foreach ($activityList as $key => $row) {
		?>
			<tr>
                <td><?php echo $row['activity_date']; ?></td>
                <td><?php echo $row['teachername']; ?></td>
				<td>
                    <p><?=$row['content']?></p>
                </td>
                <td>
                    <div style="float: left;">
                        <a download="<?=$row['docname']?>" href="upload/activity/<?=$row['filename']?>">
                        <button type="button" class="btn btn-info btn-xs">
                            <span class="glyphicon glyphicon-download">
                            </span> 
                        Download</button>
                        </a>            
                        <span>
                            <?=$row['docname']?>
                        </span>
                    </div>         
                </td>
                <td>
                    <button onclick="reloadUploader(<?=$row['id']?>)" data-toggle="modal" data-target="#actupload_<?=$key?>" 
                    type="button" class="btn btn-success btn-xs">
                            <span class="glyphicon glyphicon-upload">
                            </span> 
                        Upload My Activity
                    </button>
                    <div id="actupload_<?=$key?>" class="modal fade" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                Upload My Activity
                                </div>
                                <input type="hidden" value="<?=$row['id']?>">
                                <form role="form">
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label class="control-label">Select File</label>
                                            <input id="inputstudentactivity-7" 
                                            name="inputstudentactivity7" type="file">
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </td>
                <td>
                    <?php 
                        if ( $row['isuploaded'] == '' ) {
                    ?>
                            <button type="button" class="btn btn-info btn-xs">
                                <span class="glyphicon glyphicon-info-sign"></span>
                            </button>
                    <?php        
                        } else {
                    ?>

                            <button type="button" class="btn btn-success btn-xs">
                                <span class="glyphicon glyphicon-ok-sign"></span>
                            </button>
                    <?php        
                        }
                    ?>
                </td>
            </tr>
		<?php } ?>
    </tbody>
</table>
<table cellpadding="0" cellspacing="0" border="0" class="table  table-bordered" 
	id="example">
	<?php if( $_SESSION['levelID']  != 1){?>
    <div class="alert alert-info">
        <button type="button" class="btn btn-info" data-toggle="modal" 
        data-target="#addAnnouncement">Add Announcement</button>	 
	    <!-- Modal add announcement -->
		<?php include('app/modal/add-announcement.php');?>
    </div>
    <?php }else{ ?>
	<div style="height:20px;"></div>
    <?php	} ?>
    <thead>
    	<tr>
			<th>Announced By</th>
			<th>Grade</th>
			<th>Section</th>
			<th align="center">Date</th>
			<?php if( $_SESSION['levelID'] != 1 ){?>
			<th>Actions</th>
			<?php } ?>
		</tr>
    </thead>
    <tbody>
		<?php
		foreach ($announcementList as $key => $row) {
		?>
			<tr>
				<td style="cursor: pointer;" onclick="$('#id_tr_<?=$row['id'];?>').toggle()">
					<?php echo $row['firstname'] . ' ' . $row['lastname']; ?></td>
				<td style="cursor: pointer;" onclick="$('#id_tr_<?=$row['id'];?>').toggle()">
					<?php echo $row['usergrade']; ?></td>
				<td style="cursor: pointer;" onclick="$('#id_tr_<?=$row['id'];?>').toggle()">
					<?php echo $row['usersection']; ?></td>
				<td style="cursor: pointer;" onclick="$('#id_tr_<?=$row['id'];?>').toggle()">
					<?php echo $row['announcement_date']; ?></td>
				<?php if( $_SESSION['levelID'] != 1 ){?>
				<td>
					<!-- edit user -->
					<button type="button" disabled="disabled" class="disabled btn btn-info btn-xs" 
					data-toggle="modal" 
					data-target="#editModal<?=$row['id'];?>">
					<span class="glyphicon glyphicon-edit">
					</span> Edit</button>	 
					<?php include('app/modal/edit-user.php');?>

					<!-- delete user -->
					<button type="button" class=" btn btn-danger btn-xs" 
					data-toggle="modal" 
					data-target="#deleteModalAnnouncement<?=$row['announcementid'];?>">
					<span class="glyphicon glyphicon-trash">
					</span> Delete</button>	 
					<?php include('app/modal/delete-announcement.php');?>
				</td>
				<?php } ?>
			</tr>
			<tr id="id_tr_<?=$row['id'];?>" style="display:none">
				<td colspan="5"><?=$row['content']?></td>
			</tr>
		<?php } ?>
    </tbody>
</table>
<form role="form" id="frmAddactivity">
	<div id="div-add-row" class="">
		<select required="required" id="select-sched-teacher">
			<option value="">--Select Teacher--</option>
			<?php 
				foreach ($teachers as $key => $value) {
					echo '<option value="'.$value['id'].'">'.$value['firstname'].' '.$value['lastname'].'</option>';
				}
			?>
		</select>
		<button type="button" id="btnAddSched" class="btn btn-info">Add Schedule</button>
	</div>
		<div>
			<div id="grid"></div>
		    <div id="dialog" style="display:none">
		        <input type="hidden" id="ID">
		        <input type="hidden" id="sched-id">
		        <table id="grid-sched" border="0">
		            <tbody>
		            <tr>
		            	<td>
		            		<label for="grade">Grade</label>
		            	</td>
		            	<td>
		            		<select id="grade">
		                		<option value=""></option>
		                		<?php 
		                			foreach ($gradelevel as $key => $value) {
										echo '<option value="'.$value['id'].'">'.$value['label'].'</option>';
		                			}
		                		?>
		                	</select>
		            	</td>
		            </tr>
		            <tr>
		            	<td>
		            		<label for="section">Section</label>
		            	</td>
		            	<td>
		            		<select id="section">
		                		<option value=""></option>
		                	</select>
		            	</td>
		            </tr>
		            <tr>
		                <td><label for="subject">Subject:</label></td>
		                <td>
		                	<select id="subject">
		                		<option value="1">1</option>
		                		<option value="2">2</option>
		                	</select>
		                </td>
		            </tr>
		            <tr>
		                <td><label for="starttime">Start Time:</label></td>
		                <td><input type="text" id="starttime"></td>
		            </tr>
		            <tr>
		                <td><label for="endtime">End Time:</label></td>
		                <td><input type="text" id="endtime"></td>
		            </tr>
		        	</tbody>
		        </table>
		    </div>
		</div>	
	<div id="div-save-sched">
		<button type="submit" class="btn btn-primary">Submit</button>
	</div>
</form>
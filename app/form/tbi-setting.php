<table cellpadding="0" cellspacing="0" border="0" class="table  table-bordered" 
	id="example">
	<?php if( $_SESSION['levelID']  != 1){?>
    <div id="tbiSetting" class="alert alert-info">
    	<div>
			<button type="button" id="tbi_setting" class="btn btn-default btn-md">
		    	<span class="glyphicon glyphicon-cog">OTBI Setting</span>
			</button>
    	</div>
		<div class="alert alert-info">
			<div>
				<label for="tbi-sy">Select SY:</label>
				<select style="width:30%;" name="" class="form-control" id="tbisy">
					<?php
					for ($i=2015; $i <= 2017 ; $i++) { 
						$z = $i+1;
						$y = (string)$i .'-'.$z;
						if( $y == $getTBISetting[0]['sy']){
					?>
						<option selected="true" value="<?=$y;?>"><?=$y;?></option>
					<?php
						}else{
					?>
						<option value="<?=$y;?>"><?=$y;?></option>
					<?php } } ?>
				</select>
			</div>
			<br>
			<div>
				<label class="checkbox-inline">
				  <input type="checkbox" <?=($getTBISetting[0]['student_tbi']=='1'?'checked':'')?>  id="tbi_setting_student" 
				  data-toggle="toggle"> OTBI for student
				</label>
			</div>
			<br>
			<div>
				<label class="checkbox-inline">
				  <input type="checkbox" <?=($getTBISetting[0]['teacher_tbi']=='1'?'checked':'')?> id="tbi_setting_teacher" 
				  data-toggle="toggle"> OTBI for Teacher
				</label>
			</div>
			<br>
			<div class="">
				<button type="button" class="btn btn-default" id="tbisettingcancel">Cancel</button>
				<button type="button" class="btn btn-primary" id="tbisettingsave">
					<span class="glyphicon glyphicon-saved">Save</span>
				</button>
			</div>
		</div>
    </div>
    <?php }else{ ?>
	<div style="height:20px;"></div>
    <?php	} ?>
    <thead>
    	<tr>
			<th>First Name</th>
			<th>Last Name</th>
			<th>TBI Grade</th>
		</tr>
    </thead>
    <tbody>
		<?php
		foreach ($allUsers as $key => $row) {
		?>
			<tr style="cursor: pointer;" onclick="$('#id_tr_<?=$key;?>').toggle()">
				<td><?php echo $row['firstname']; ?></td>
				<td><?php echo $row['lastname']; ?></td>
				<td><?php echo $row['usergrade']; ?></td>
			</tr>
			<tr id="id_tr_<?=$key;?>" style="display:none">
				<td colspan="5"><?=$row['content']?></td>
			</tr>
		<?php } ?>
    </tbody>
</table>
<div></div>

<form role="form" id="addtbi">
	<div class="form-group">
	<label for="teacherlist">Teacher list:</label>
	<select class="form-control" id="teacherlist">		
		<?php
			foreach ($getTeachersForTBIListT as $key => $value) {
				echo '<option value="'.$value['id'].'">'.$value['firstname'] .' '. $value['lastname'] .'</option>';
			}
		?>
	</select>
	</div>
	<table id="tblGrades" 
	class="table table-condensed table-bordered table-striped volumes">
	    <tbody>
	        <tr>
	            <th>Question</th>
	            <th>Answer</th>
	        </tr>
	        <?php
	        	foreach ($questions as $key => $value) {
	        ?>
	        <tr>
	            <th>
	            	<?php 
	            	$label = $value['question'];
	            	echo $label;
	            	?>
	            	<input name="question_id" id="question_id" type="hidden" value="<?=$value['id'];?>">
	            </th>
	            <td>
	            	<?php foreach ($tbiAnswerList as $tbikey => $tbivalue) { ?>
						<span class="answerlist">
							<input type="radio" name="dataAnswer_<?=$value['id']?>" 
							value="<?=$tbivalue['answer_value']?>" ><?=$tbivalue['answer_value']?>
						</span>
	            	<?php } ?>
	            </td>
	        </tr>
	        <?php 
	        	}
	        ?>
	    </tbody>
	</table>
	<div class="contrainer-fluid">
		<div class="row">
			<div class="col-md-11"></div>
			<div class="col-md-1">
				<button type="button" id="btnAddTBI" class="btn btn-primary">Save</button>
			</div>
		</div>
	</div>	
</form>

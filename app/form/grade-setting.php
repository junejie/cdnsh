<style type="text/css">
	#grade-setting{
		padding-top: 20px;
	}
	div#grade-setting {
	    display: none;
	}
</style>
<div id="grade-setting">
	<form name="grade-setting">
		<label for="activeteacher"> Activate Teacher for Grading </label>
		<select multiple="multiple" size="10" name="activeteacher" class="select-setting-grade">
		<?php
		foreach ($getGradeSetting as $key => $value) {
			echo '<option '.( $value['status'] == '1' ? ' selected="selected" ':'' )
			.' " value="'.$value['t_id'].'">'.$value['fullname'].'</option>';
		}
		?>
		</select>
		<div class="form-group" style="padding: 20px;">
			<button type="button" id="grade-setting-save" style="margin-right:20px" class="btn btn-primary">Save</button>
			<button type="button" id="grade-setting-cancel" class="btn btn-default">Cancel</button>
		</div>
	</form>
</div>
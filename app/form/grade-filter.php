<?php
	$grade = schedGrade($connection);
	array_unshift($grade, array( id=> '0', 'label'=>"--Please Select--") );
	$section = array();
	$subject = array();

?>

<div class="container-fluid bg-info">
	<div class="row">
		<div class="col-xs-10">
			<div class="col-xs-3">
				<div class="form-group">
					<label for="dataYear">Year</label>
					<select id="dataYear" class="form-control">
					<?php
						foreach ($grade as $key => $value) {
							echo '<option value="'.$value['id'].'">'.$value['label'].'</option>';
						}
					?>
					</select>
				</div>
			</div>
			<div class="col-xs-3">
				<div class="form-group">
					<label for="dataSection">Section</label>
					<select id="dataSection" class="form-control">
					<?php
						foreach ($section as $key => $value) {
							echo '<option value="'.$value['id'].'">'.$value['label'].'</option>';
						}
					?>
					</select>
				</div>
			</div>
			<div class="col-xs-3">
				<div class="form-group">
					<label for="dataSubject">Subject</label>
					<select id="dataSubject" class="form-control">
					<?php
						foreach ($subject as $key => $value) {
							echo '<option value="'.$value['id'].'">'.$value['label'].'</option>';
						}
					?>
					</select>
				</div>
			</div>
			<div class="col-xs-3">
				<div class="form-group">
					<label for="">Search</label>
					<button type="button" id="btnSearchGrade" 
					class="form-control btn btn-primary glyphicon glyphicon-search">Search</button>
				</div>
			</div>
		</div>
		<div class="col-xs-2">
			<?php
			//for it admin only
			if ( $_SESSION['levelID'] == 3) {
			?>
			<div class="col-xs-1">
				<div class="form-group">
					<label for="btnGradeSetting" style="height:15px;"></label>
					<button name="btnGradeSetting" type="button" id="btnGradeSetting" 
					class="form-control btn btn-primary glyphicon glyphicon-wrench"></button>
				</div>
			</div>
			
			<?php  } ?>
		</div>	
	</div>
</div>
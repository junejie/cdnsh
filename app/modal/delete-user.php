<div id="deleteModal<?=$row['id'];?>" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<form role="form" id="deleteuser">
				<div class="modal-header">
					<h4 class="modal-title">Delete User</h4>
				</div>
				<div class="modal-body alert alert-danger">
					<div class="form-group">
						<p>Are you sure you want to delete <?=$row['username'];?> user?</p>
						<input type="hidden" class="form-control" id="deleteusername" 
						placeholder="User Name" value="<?=$row['username']; ?>">
					</div>
				</div>	
				<div class="modal-footer">
					<button type="button" 
					onclick="deleteUser('<?=$row['id'];?>')" class="btn btn-primary">Submit
					</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close
					</button>
				</div>
			</form>
		</div>
	</div>
</div>
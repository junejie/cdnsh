<style>
	.form-group {
	    display: table !important;
	}

	.form-group > label {
	    display: table;
	}

	.form-group > input {
	    width: 100% !important;
	}

	.form-group > select {
	    display: block !important;
	    width: 100% !important;
	}

	.form-inline .form-group {
	    width: 100% !important;
	}
</style>
<?php 

$data = getUserType($connection,$where);
?>
<div id="editModal<?=$row['id'];?>" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<!-- Modal content-->
		<div class="modal-content">
			<form role="form" id="edituser">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Edit User</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="username">User Type</label>
						<select id="usertype" class="form-control">
							<?php
								foreach ($data as $key => $value) {
									echo '<option value="'.$value['id'].'">'.$value['user_type'].'</option>';
								}
							?>
						</select>
					</div>
					<div class="form-group">
						<label for="username">User Name</label>
						<input type="text" class="form-control" id="username" placeholder="User Name" value="<?=$row['username'];?>">
					</div>
					<div class="form-group">
						<label for="firstname">Firstname</label>
						<input type="text" class="form-control" id="firstname" placeholder="Firstname" value="<?=$row['firstname'];?>">
					</div>
					<div class="form-group">
						<label for="lastname">Lastname</label>
						<input type="text" class="form-control" id="lastname" placeholder="Lastname" value="<?=$row['lastname'];?>">
					</div>
					<div class="form-group">
						<label for="email">Email</label>
						<input type="email" class="form-control" id="email" placeholder="Email" value="<?=$row['email'];?>">
					</div>
					<div class="form-group">
						<label for="password">Password</label>
						<input type="password" class="form-control" id="password" placeholder="Password" 
						value="<?=$row['password'];?>">
						<input type="hidden" id="id" value="<?=$row['id'];?>">
					</div>
				</div>	
				<div class="modal-footer">
					<button type="button" onclick="editUser(<?=$row['id'];?>)" class="btn btn-primary">Submit</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>
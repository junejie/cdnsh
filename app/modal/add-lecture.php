<?php
	require_once('Connections/conn.php');
	include_once('app/proc/User.php');
	$connection = db_connect();

	//it admin
	$isSuperUser = false;
	if ($_SESSION['levelID'] == '2') {
		$where = " group_user != 'superuser'";
	
	//superuser
	}elseif($_SESSION['levelID'] == '7'){
		$where = "group_user = 'admin'";
		$isSuperUser = true;
	
	//registrar
	}elseif($_SESSION['levelID'] == '5'){
		$where = "group_user = 'student'";
	
	}else{
		$where = '0';

	}
	
	$data = getUserType($connection,$where);
?>
<link rel="stylesheet" href="assets/css/add-lecture.css">
<div id="addLecture" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<form role="form" id="frmAddLecture">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Add Lecture</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<div class="container-fluid">
							<div class="row">
								<div class="col-md-6" style="padding-left: 0px; ">
									<label for="lectureUserYear">Year</label>
									<select onchange="onChangeYear()" id="lectureUserYear" class="form-control" 
									<?php if( $isSuperUser ){ echo 'disabled="disabled"'; } ?>  
									>
										<?php
											foreach ($grade as $key => $value) {
												echo '<option value="'.$value['id'].'">'.$value['label'].'</option>';
											}
										?>
									</select>
								</div>	
								<div class="col-md-6">
									<label for="lectureUserSection">Section</label>
									<select id="lectureUserSection" class="form-control" 
									<?php if( $isSuperUser ){ echo 'disabled="disabled"'; } ?>  
									>
										<?php
											foreach ($section as $key => $value) {
												echo '<option value="'.$value['id'].'">'.$value['label'].'</option>';
											}
										?>
									</select>
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<div>
							<label for="lectureSubject">Subject</label>
							<select id="lectureSubject" class="form-control" >
								<?php
									foreach ( [''] as $key => $value) {
										echo '<option value="0	">-</option>';
									}
								?>
							</select>
						</div>	
						
					</div>
					<div class="form-group">
						<label for="lecturedate">Lecture Date</label>
						<input type="date" class="form-control" id="lecturedate" placeholder="User Name" value="">
					</div>
					<div class="form-group">
						<label for="lecturetitle">Title</label>
						<input  class="form-control" 
						name="lecturetitle" id="lecturetitle" 
						cols="60" rows="10" />
					</div>
					<div class="form-group">
						<label class="control-label">Select File</label>
						<input id="inputlecture-7" name="inputlecture7" type="file">
					</div>
				</div>	
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Submit</button>
					<button type="button" class="btn btn-default" 
					data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>

<?php
	require_once('Connections/conn.php');
	include_once('app/proc/User.php');
	$connection = db_connect();

	//it admin
	$isSuperUser = false;
	if ($_SESSION['levelID'] == '2') {
		$where = " group_user != 'superuser'";
	
	//superuser
	}elseif($_SESSION['levelID'] == '7'){
		$where = "group_user = 'admin'";
		$isSuperUser = true;
	
	//registrar
	}elseif($_SESSION['levelID'] == '5'){
		$where = "group_user = 'student'";
	
	//a-it
	}elseif( $_SESSION['levelID'] == '3' ){
		$where = "group_user = 'faculty'";

	}else{
		$where = '0';

	}
	
	$data = getUserType($connection,$where);
?>
<style>
	.form-group {
	    display: table !important;
	}

	.form-group > label {
	    display: table;
	}

	.form-group > input {
	    width: 100% !important;
	}

	.form-group > select {
	    display: block !important;
	    width: 100% !important;
	}

	.form-inline .form-group {
	    width: 100% !important;
	}
</style>
<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<form role="form" id="adduser">

				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Add User</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<div class="container-fluid">
							<div class="row">
								<div class="col-md-4">
									<label for="usertype">User type</label>
									<select id="usertype" class="form-control">
										<?php
											foreach ($data as $key => $value) {
												echo '<option value="'.$value['id'].'">'.$value['user_type'].'</option>';
											}
										?>
									</select>
								</div>
								<div class="col-md-4">
									<label for="userYear">Year</label>
									<select id="userYear" class="form-control" 
									<?php if( $isSuperUser ){ echo 'disabled="disabled"'; } ?>  
									>
										<?php
											foreach ($grade as $key => $value) {
												echo '<option value="'.$value['id'].'">'.$value['label'].'</option>';
											}
										?>
									</select>
								</div>	
								<div class="col-md-4">
									<label for="userSection">Section</label>
									<select id="userSection" class="form-control" 
									<?php if( $isSuperUser ){ echo 'disabled="disabled"'; } ?>  
									>
										<?php
											foreach ($section as $key => $value) {
												echo '<option value="'.$value['id'].'">'.$value['label'].'</option>';
											}
										?>
									</select>
								</div>	
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="username">User Name</label>
						<input type="text" class="form-control" id="username" placeholder="User Name" value="">
					</div>
					<div class="form-group">
						<label for="firstname">Firstname</label>
						<input type="text" class="form-control" id="firstname" placeholder="Firstname" value="">
					</div>
					<div class="form-group">
						<label for="lastname">Lastname</label>
						<input type="text" class="form-control" id="lastname" placeholder="Lastname" value="">
					</div>
					<div class="form-group">
						<label for="email">Email</label>
						<input type="email" class="form-control" id="email" placeholder="Email" value="">
					</div>
					<div class="form-group">
						<label for="password">Password</label>
						<input type="password" class="form-control" id="password" placeholder="Password" value="">
					</div>
				</div>	
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Submit</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>
<div id="msgSubmit" class="h3 text-center hidden">Message Submitted!</div>
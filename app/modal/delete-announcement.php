<div id="deleteModalAnnouncement<?=$row['announcementid'];?>" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<form role="form" id="deleteuser">
				<div class="modal-header">
					<h4 class="modal-title">Delete Announcement</h4>
				</div>
				<div class="modal-body alert alert-danger">
					<div class="form-group">
						<p>Are you sure you want to delete  Announcement?</p>
						<input type="hidden" class="form-control" id="deleteusername" 
						placeholder="Announcement Name" value="<?=$row['username']; ?>">
					</div>
				</div>	
				<div class="modal-footer">
					<button type="button" 
					onclick="delAnnouncement('<?=$row['announcementid'];?>')" class="btn btn-primary">Submit
					</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Close
					</button>
				</div>
			</form>
		</div>
	</div>
</div>
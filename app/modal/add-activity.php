<?php
	require_once('Connections/conn.php');
	include_once('app/proc/User.php');
	$connection = db_connect();

	//it admin
	$isSuperUser = false;
	if ($_SESSION['levelID'] == '2') {
		$where = " group_user != 'superuser'";
	
	//superuser
	}elseif($_SESSION['levelID'] == '7'){
		$where = "group_user = 'admin'";
		$isSuperUser = true;
	
	//registrar
	}elseif($_SESSION['levelID'] == '5'){
		$where = "group_user = 'student'";
	
	}else{
		$where = '0';

	}
	
	$data = getUserType($connection,$where);
?>
<div id="addActivity" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<form role="form" id="frmAddactivity">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Add Activity</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="activityUserYear">Year</label>
						<select id="activityUserYear" class="form-control" 
						<?php if( $isSuperUser ){ echo 'disabled="disabled"'; } ?>  
						>
							<?php
								foreach ($grade as $key => $value) {
									echo '<option value="'.$value['id'].'">'.$value['label'].'</option>';
								}
							?>
						</select>
					</div>
					<div class="form-group">
						<label for="activityUserSection">Section</label>
						<select id="activityUserSection" class="form-control" 
						<?php if( $isSuperUser ){ echo 'disabled="disabled"'; } ?>  
						>
							<?php
								foreach ($section as $key => $value) {
									echo '<option value="'.$value['id'].'">'.$value['label'].'</option>';
								}
							?>
						</select>
					</div>
					<div class="form-group">
						<label for="activitydate">Activity Date</label>
						<input type="date" class="form-control" id="activitydate" placeholder="User Name" value="">
					</div>
					<div class="form-group">
						<label for="activitystatus">Status</label>
						<select id="activitystatus" class="form-control" >
							<option value="open">Open</option>
							<option value="close">Close</option>
						</select>
					</div>
					<div class="form-group">
						<label for="activitycontent">Content</label>
						<input type="text" class="form-control" 
						name="activitycontent" id="activitycontent" />
					</div>
					<div class="form-group">
						<label class="control-label">Select File</label>
						<input id="input-7" name="input7" type="file">
						<div id="errorBlock" class="help-block"></div>
					</div>
				</div>	
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Submit</button>
					<button type="button" class="btn btn-default" 
					data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>

<?php
	require_once('Connections/conn.php');
	include_once('app/proc/User.php');
	$connection = db_connect();

	//it admin
	$isSuperUser = false;
	if ($_SESSION['levelID'] == '2') {
		$where = " group_user != 'superuser'";
	
	//superuser
	}elseif($_SESSION['levelID'] == '7'){
		$where = "group_user = 'admin'";
		$isSuperUser = true;
	
	//registrar
	}elseif($_SESSION['levelID'] == '5'){
		$where = "group_user = 'student'";
	
	}else{
		$where = '0';

	}
	
	$data = getUserType($connection,$where);
?>
<style>
	.form-group {
	    display: table !important;
	}

	.form-group > label {
	    display: table;
	}

	.form-group > input {
	    width: 100% !important;
	}

	.form-group > select {
	    display: block !important;
	    width: 100% !important;
	}

	.form-inline .form-group {
	    width: 100% !important;
	}
</style>
<div id="addAnnouncement" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<form role="form" id="frmAddAnnouncement">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Add Announcement</h4>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="announcementUserYear">Year</label>
						<select id="announcementUserYear" class="form-control" 
						<?php if( $isSuperUser ){ echo 'disabled="disabled"'; } ?>  
						>
							<?php
								foreach ($grade as $key => $value) {
									echo '<option value="'.$value['id'].'">'.$value['label'].'</option>';
								}
							?>
						</select>
					</div>
					<div class="form-group">
							<label for="announcementUserSection">Section</label>
							<select id="announcementUserSection" class="form-control" 
							<?php if( $isSuperUser ){ echo 'disabled="disabled"'; } ?>  
							>
								<?php
									foreach ($section as $key => $value) {
										echo '<option value="'.$value['id'].'">'.$value['label'].'</option>';
									}
								?>
							</select>
					</div>
					<div class="form-group">
						<label for="announcementdate">Announcement Date</label>
						<input type="date" class="form-control" id="announcementdate" placeholder="User Name" value="">
					</div>
					<div class="form-group">
						<label for="announcementcontent">Content</label>
						<textarea  class="form-control" 
						name="announcementcontent" id="announcementcontent" 
						cols="60" rows="10"></textarea>
					</div>
				</div>	
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Submit</button>
					<button type="button" class="btn btn-default" 
					data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>

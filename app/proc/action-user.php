<?php

require_once('../../Connections/conn.php');
include_once('User.php');
$connection = db_connect();

$action = $_GET['action'];

$body = file_get_contents('php://input');
//var_dump($_POST);
//var_dump($body);

if ($action == 'deleteuser') {

	echo deleteUser($connection,$_GET['username']);

}elseif($action == 'edituser'){

	echo editUser($connection,$_POST);

}elseif($action == 'adduser'){

	echo addUser($connection,$_POST);

}elseif ($action == 'savegrade') {

	echo saveGrade($connection,$body);

}elseif( $action == 'addannouncement'){

	echo addAnnouncement($connection,$_POST);

}elseif( $action == 'addtbi'){

	echo addTBI($connection, $body);

}elseif( $action == 'tbisetting' ){

    echo saveTBISetting($connection,$body);

}elseif( $action == 'addactivity' ){

    echo addactivity($connection,$_POST);

}elseif( $action == 'deleteactivity' ){

    echo deleteActivity($connection,$_POST);

}elseif( $action == 'getstudentactivities' ){

    echo getStudentActivities($connection,$_GET);

}elseif( $action == 'updateactivity' ){

	echo updateActivity($connection,$_POST);

}elseif ( $action == 'gradesetting' ) {

	echo updateGradeSetting($connection,$_POST);

}elseif ( $action == 'delannouncement' ){

	echo delAnnouncement($connection);

}elseif ( $action == 'addlecture' ) {
	
	echo addlecture($connection,$_POST);

}elseif ( $action == 'deletelecture' ) {
	
	echo deleteLecture($connection,$_POST);

}elseif ( $action == 'getsubject' ) {
	$subject = getSubject($connection);
	foreach ($subject as $key => $value) {
		echo '<option value="'.$value['id'].'">'. $value['label'] .'</option>';
	}

}elseif ( $action == 'sectionbygrade' ) {
	$v = getSectionsByGrade($connection);
	foreach ($v as $key => $value) {
		echo '<option value="'.$value['id'].'">'. $value['label'] .'</option>';
	}

}elseif ( $action == 'subjectbygrade' ) {
	$v = getSubjectByGrade($connection);
	foreach ($v as $key => $value) {
		echo '<option value="'.$value['id'].'">'. $value['label'] .'</option>';
	}
}elseif ( $action == 'sectionbygradesched' ) {
	$v = getSectionsByGradeSched($connection);
	foreach ($v as $key => $value) {
		echo '<option value="'.$value['id'].'">'. $value['label'] .'</option>';
	}

}elseif ( $action == 'subjectbygradesched' ) {
	$v = getSubjectByGradeSched($connection);
	foreach ($v as $key => $value) {
		echo '<option value="'.$value['id'].'">'. $value['label'] .'</option>';
	}
}elseif ( $action == 'addschedule' ) {

	header('Content-Type: application/json');
	echo addSchedule($connection);

}elseif ( $action == 'updateschedule' ) {

	header('Content-Type: application/json');
	echo updateSchedule($connection);

}elseif ( $action == 'deleteschedule' ) {

	header('Content-Type: application/json');
	echo deleteSchedule($connection);

}elseif ( $action == 'getschedule' ) {
	header('Content-Type: application/json');
	echo getSchedule($connection);

}elseif ( $action == 'getevents' ) {
	header('Content-Type: application/json');
	echo getEvents($connection);

}elseif ( $action == 'addevents' ) {
	header('Content-Type: application/json');
	echo addEvents($connection,$body);

}elseif ( $action == 'deleteevents' ) {
	header('Content-Type: application/json');
	echo deleteEvents($connection);

}else {

	echo 'invalid action';

}

$connection->close();

?>
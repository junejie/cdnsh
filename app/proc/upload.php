<?php

require_once('../../Connections/conn.php');
include_once('User.php');

$action = $_GET['action'];

if ( isset( $_FILES['input7']) ) { 
    $f_type = $_FILES['input7']['type'];
}elseif (isset($_FILES['inputstudentactivity7'])) {    
    $f_type = $_FILES['inputstudentactivity7']['type'];
}

$valid_type = array(
    'application/msword',
    'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    'application/vnd.openxmlformats-officedocument.wordprocessingml.template',
    'application/vnd.ms-word.document.macroEnabled.12',
    'application/vnd.ms-excel',
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    'application/vnd.openxmlformats-officedocument.spreadsheetml.template',
    'application/vnd.ms-excel.sheet.macroEnabled.12',
    'application/vnd.ms-excel.template.macroEnabled.12',
    'application/vnd.ms-excel.addin.macroEnabled.12',
    'application/vnd.ms-excel.sheet.binary.macroEnabled.12',
    'application/vnd.ms-powerpoint',
    'application/vnd.openxmlformats-officedocument.presentationml.presentation',
    'application/vnd.openxmlformats-officedocument.presentationml.template',
    'application/vnd.openxmlformats-officedocument.presentationml.slideshow',
    'application/vnd.ms-powerpoint.addin.macroEnabled.12',
    'application/vnd.ms-powerpoint.presentation.macroEnabled.12',
    'application/vnd.ms-powerpoint.template.macroEnabled.12',
    'application/pdf',
    'application/vnd.ms-powerpoint.slideshow.macroEnabled.12'
);

$validFile = array_search($f_type, $valid_type);
$isFileOK = 'true';
if ($validFile == false) {
    $isFileOK = 'false';
}


//ACTIVITY UPLOAD OF TEACHER
if ( $action == 'teacherupload') {
    $savefolder = '../../upload/activity/';
    $docname = $_FILES['input7']['name'];
    $filename =  basename( $_FILES['input7']['tmp_name']); 
    $file = $_FILES['input7']['tmp_name'];
    try{
        $u = move_uploaded_file( $_FILES['input7']['tmp_name'] , $savefolder.$filename);
    }catch (Exception $e) {
        echo $e;
    }
    $status = ($u==true)?'true':'false';
    $data = '{
        "status": "'.$status.'",
        "docname": "'.$docname.'",
        "filename": "'.$filename.'",
        "isFileOK": "'.$isFileOK.'"
    }';
    echo $data;
    
//ACTIVITY UPLOAD OF STUDENT
}elseif( $action == 'studentupload' ){
    $savefolder = '../../upload/activity-student/';
    $docname = $_FILES['inputstudentactivity7']['name'];
    $filename =  basename( $_FILES['inputstudentactivity7']['tmp_name']); 
    $file = $_FILES['inputstudentactivity7']['tmp_name'];
    try{
        $u = move_uploaded_file( $_FILES['inputstudentactivity7']['tmp_name'] , $savefolder.$filename);
    }catch (Exception $e) {
        echo $e;
    }
    $status = ($u==true)?'true':'false';
    if ($u) {
        
        //INSERT FILE UPLOAD TO DB

        $connection = db_connect();

        $uploadStatus = uploadActivity($connection,$_FILES,$_POST);
        if ($uploadStatus != 'success') {
            $status = 'false';
        }

        $connection->close();

    }
    $data = '{
        "status": "'.$status.'",
        "docname": "'.$docname.'",
        "filename": "'.$filename.'",
        "isFileOK": "'.$isFileOK.'"
    }';
    echo $data;    


//TEACHER UPLOAD LECTURE
}elseif( $action == 'teacheruploadlecture' ){

    $savefolder = '../../upload/lecture/';
    $docname = $_FILES['inputlecture7']['name'];
    $filename =  basename( $_FILES['inputlecture7']['tmp_name']); 
    $file = $_FILES['inputlecture7']['tmp_name'];
    try{
        $u = move_uploaded_file( $_FILES['inputlecture7']['tmp_name'] , $savefolder.$filename);
    }catch (Exception $e) {
        echo $e;
    }
    $status = ($u==true)?'true':'false';
    $data = '{
        "status": "'.$status.'",
        "docname": "'.$docname.'",
        "filename": "'.$filename.'",
        "isFileOK": "'.$isFileOK.'"
    }';
    echo $data;

}else {
    echo 'invalid action';
}

?>
<?php

function permissions($permissiontype){
    /*
    *
    values can be 
    useradd,userdelete,useredit
    *
    *
    *
    */
    $r  = '{
        "1":[
            "banner",
            "calendar-view",
            "js-calendar-view"
        ],
        "2":[
            "banner",
            "tbisettingtab",
            "calendar-admin",
            "js-calendar",
            "lectures-tab",
            "lectures-table"
        ],
        "3":[
            "banner",
            "useradd",
            "userdelete",
            "useredit",
            "calendar-admin",
            "js-calendar",
            "lectures-tab",
            "lectures-table"
        ],
        "4":[
            "banner"
        ],
        "5":[
            "banner",
            "calendar-view",
            "js-calendar-view"
        ],
        "6":[
            "banner",
            "calendar-admin",
            "js-calendar"
        ],
        "7":[
            "banner"
        ]
    }';
    $json = json_decode($r,true);
    $currentPermission = $json[ $_SESSION['levelID'] ] ;
    $s = array_search($permissiontype, $currentPermission);
    if($s !== false){
        if ( $permissiontype == 'useradd') {
            $h = <<<PHP
             <button type="button" class="btn btn-info" data-toggle="modal" 
            data-target="#myModal">Add User</button>
PHP;
            echo $h;

            
        }elseif ( $permissiontype == 'tbisettingtab') {
            $h = <<<PHP
            <li>
                <a data-toggle="tab" href="#tab6">OTBI Results</a>
            </li>
PHP;
            echo $h;
        }elseif ( $permissiontype == 'calendar-admin') {
            $h = <<<PHP
            <div id='calendar'></div>
PHP;
            echo $h;
        }elseif ( $permissiontype == 'banner') {
            $h = <<<PHP
                <div class="container">
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                            <li data-target="#myCarousel" data-slide-to="1"></li>
                            <li data-target="#myCarousel" data-slide-to="2"></li>
                            <li data-target="#myCarousel" data-slide-to="3"></li>
                        </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <div class="item active">
                              <img src="assets/images/cropped-Banner1.jpg" alt="Chania">
                            </div>

                            <div class="item">
                              <img src="assets/images/cropped-Banner1.jpg" alt="Chania">
                            </div>

                            <div class="item">
                              <img src="assets/images/cropped-Banner1.jpg" alt="Flower">
                            </div>

                            <div class="item">
                              <img src="assets/images/cropped-Banner1.jpg" alt="Flower">
                            </div>
                        </div>

                        <!-- Left and right controls -->
                        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
PHP;
            echo $h;
        }elseif ( $permissiontype == 'calendar-view') {
            $h = <<<PHP
            <div id='calendar-view'></div>
PHP;
            echo $h;
        }elseif ( $permissiontype == 'js-calendar-view') {
            $h = <<<PHP
            <script src='assets/js/calendar-view.js'></script>
PHP;
            echo $h;
        }elseif ( $permissiontype == 'js-calendar') {
            $h = <<<PHP
            <script src='assets/js/calendar.js'></script>
PHP;
            echo $h;
        }elseif ( $permissiontype == "lectures-tab") {
            $h = <<<PHP
            <li>
                <a data-toggle="tab" href="#lectures-tab">Lectures</a>
            </li>
PHP;
            echo $h;
        }

    }else{

        //if principal
        if ( $permissiontype == 'useredit') {
            return 'disabled';
        }elseif( $permissiontype == 'userdelete' ){
            return 'disabled';
        }
    }
}

?>
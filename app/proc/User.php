<?php

function deleteEvents($connection){
	var_dump($_GET);

	$id = mysqli_real_escape_string($connection, $_GET['id']);
	$delete = "
		delete from calendar where id = '$id'
	";
	$delete = $connection->query($delete);

	if ( $delete === TRUE ) {
	    $status = 1;
	} else {
	    echo "Error: " . $sql . "<br>" . $connection->error;
	    $status = 0;
	}
	$r = array('status'=> $status);
	return json_encode($r);
}

function addEvents($connection,$body){
	$data = json_decode($body,true);
	$user = $_SESSION['id'];

	$start = mysqli_real_escape_string($connection, $data['start']);
	$end = mysqli_real_escape_string($connection, $data['end']);
	$title = mysqli_real_escape_string($connection, $data['title']);
	$content = mysqli_real_escape_string($connection, $data['content']);
	$update = "
		INSERT INTO `calendar`(  `startdate`, `enddate`, `to_year`, `to_section`, `saved_by`, `title`, `content`) VALUES (
			'$start', '$end','','','$user', '$title','$content'
		)
	";
	$insert = $connection->query($update);

	if ( $insert === TRUE ) {
	    $status = 1;
	} else {
	    echo "Error: " . $sql . "<br>" . $connection->error;
	    $status = 0;
	}
	$r = array('status'=> $status);
	return json_encode($r);
}
function getEvents($connection){
	$where = "";
	if( $_SESSION['usersection'] && $_SESSION['useryear']){
		$y = $_SESSION['useryear'];
		$s = $_SESSION['usersection'];
		$where = " 
        where announce_to_year = '$y'
        and announce_to_section = '$s' ";
	}
	$query = "
		 SELECT 
			 id,
			 title, 
			 startdate as start , 
			 IF( enddate = '0000-00-00 00:00:00','', enddate)  as end,
			 content,
			 'e' as type_calendar
			 FROM calendar
		union 
		SELECT 
			id,
			'Announcement' as title,
			announcement_date as start,
			'' as end,
			content,
			'a' as type_calendar
		FROM `announcement`

		" .$where;
    $data = [];
    $result = mysqli_query($connection,$query);
    while($row = $result->fetch_assoc()) {
    	if ($row['end'] != '') {
        	$data[]=$row;
    	}else{
    		$row = array( 
    			'id'=>$row['id'], 
    			'content'=>$row['content'], 
    			'start'=>$row['start'], 
    			'title'=>$row['title'],
    			'type_calendar'=>$row['type_calendar'] 
    		);
        	$data[]=$row;
    	}
    }
    $data = json_encode($data);
    return $data;
}

function getAllUsers($connection){
	$query = "
		 SELECT 
			firstname,
			lastname,
			teacher_id , 
			avg(user_answer_val)  as usergrade
		FROM tbi_answer
		left join tbl_user
		on tbi_answer.teacher_id = tbl_user.id
		group by teacher_id
		";
    $data = [];
    $result = mysqli_query($connection,$query);
    while($row = $result->fetch_assoc()) {
        $data[]=$row;
    }	
    return $data;
}

function getAssignedLecture($connection){
	$usersection = $_SESSION['usersection'];
	$query = "
		 SELECT 
			lectures.id as id,
			lectures.saved_ts as saved_ts,
			user_subject.label as sectionlabel,
    		lectures.title as title,
		    docname,
		    filename
		FROM `lectures`
		left join user_subject
		on user_subject.id =subjectid
		where sectionid = '$usersection'
		order by user_subject.label desc
		";
    $data = [];
    $result = mysqli_query($connection,$query);
    while($row = $result->fetch_assoc()) {
        $data[]=$row;
    }	
    return $data;
}

function getlectureList($connection){
	$currentUser = $_SESSION['id'];
	$query = "
		SELECT 
				lectures.id as id, 
				lectures.yearid as  yearid, 
				lectures.sectionid as  sectionid, 
				lectures.subjectid as subjectid, 
				lectures.title as  title, 
				lectures.added_by as  added_by, 
				lectures.docname as docname, 
				lectures.filename as filename,
				user_grade.label as gradelabel,
				saved_ts,
				user_section.label as sectionname,
                user_subject.label as subjectname
		FROM lectures 
        
        LEFT JOIN user_grade 
        on yearid= user_grade.id 
        
        LEFT JOIN user_section
        on lectures.sectionid = user_section.id
        
        LEFT JOIN user_subject
        on user_subject.id = lectures.subjectid
        
        WHERE added_by = '$currentUser'
		";
    $data = [];
    $result = mysqli_query($connection,$query);
    while($row = $result->fetch_assoc()) {
        $data[]=$row;
    }	
    return $data;
}

function getIsSaveGrade($connection){
	$levelid = $_SESSION['id'];
	$query = "
		SELECT isenable FROM grade_setting
		WHERE teacher_id = '$levelid'
		";
    $data = [];
    $result = mysqli_query($connection,$query);
    while($row = $result->fetch_assoc()) {
        $data[]=$row;
    }	
    return $data;
}

function updateGradeSetting($connection,$post){
	try{
		//DELETE DATA
		$delete = "
			delete from grade_setting ;
		";
		$delete = $connection->query($delete);
		if ( $delete !== TRUE) {
			echo "Error: " . $sql . "<br>" . $connection->error;
		}

		//SELECTED ITEM INSERT
		if ( $post['data'] != NULL) {
			# code...
			foreach ($post['data'] as $key => $value) {
				$id = mysqli_real_escape_string($connection, $value );
				$insert = "
					INSERT INTO grade_setting (
					 teacher_id, isenable) 
					 VALUES ( '$value', '1');
				";
				$insert = $connection->query($insert);
				if ( $insert !== TRUE) {
					echo "Error: " . $sql . "<br>" . $connection->error;
				}
			}
		}

		//UPDATE NON SELETED ITEM
		if ( $post['data_nonselected'] != NULL ) {
			foreach ($post['data_nonselected'] as $key => $value) {
				$id = mysqli_real_escape_string($connection, $value );
				$insert = "
					INSERT INTO grade_setting (
					 teacher_id, isenable) 
					 VALUES ( '$value', '0');
				";
				$insert = $connection->query($insert);
				if ( $insert !== TRUE) {
					echo "Error: " . $sql . "<br>" . $connection->error;
				}
			}
		}
		echo 'success';
	} catch (Exception  $e){
		echo 'Caught exception: ',  $e->getMessage(), "\n";
	}
}

function getGradeSetting($connection){
	$query = "
		SELECT 
			tbl_user.id as t_id,
			concat( firstname,' ',lastname ) as fullname,
			if (grade_setting.isenable =0, false,true ) as status
		FROM `tbl_user`
		left join grade_setting
		on tbl_user.id =  grade_setting.teacher_id
		where levelID != 1
		or levelID != 7
		or levelID != 2
		order by levelID
		";
    $data = [];
    $result = mysqli_query($connection,$query);
    while($row = $result->fetch_assoc()) {
        $data[]=$row;
    }
    return $data;

}
function getStudentActivities($connection,$get){
	$id = mysqli_real_escape_string($connection,$get['activity']);
	$query = "
		SELECT
			concat(firstname,' ', lastname) as studentname,
			ts,
			docname,
			filename
		FROM activities_answer
		LEFT join tbl_user
		on fk_studentid = tbl_user.id
		where fk_activity = '$id'";
	$table = "
	<table class='table'> 
		<thead> 
			<tr> 
				<th>Uploaded By</th> 
				<th>Date Time</th> 
				<th>Doc Name</th>
				<th>Download</th>

			</tr> 
		</thead> 
		<tbody> ";
    $data = [];
    $result = mysqli_query($connection,$query);
    while($row = $result->fetch_assoc()) {
        $table .= "<tr> <td>".$row['studentname']."</td>";
        $table .= "<td>".$row['ts']."</td>";
        $table .= "<td>".$row['docname']."</td>";
        $table .= "<td>
			<a download=\"".$row['docname'] ."\" 
			href=\"upload/activity-student/".$row['filename']."\">
			<button type=\"button\" class=\"btn btn-info btn-xs\">
			<span class=\"glyphicon glyphicon-download\">
			</span> 
			Download</button>
			</a>
        </td> </tr> ";
    } 
    $table .= "</tbody></table>";
    return $table;
}

function updateActivity($connection,$post){
	$id = mysqli_real_escape_string($connection, $post['id']);
	$status = mysqli_real_escape_string($connection, $post['status']);
	$update = "
		update activities 
		set status = '$status'
		where id = '$id';
	";
	$insert = $connection->query($update);

	if ( $insert === TRUE ) {
	    return 'success';
	} else {
	    return "Error: " . $sql . "<br>" . $connection->error;
	}
}

function uploadActivity($connection,$files,$post){
	$fk_activity = mysqli_real_escape_string($connection, $post['d1']);
	$fk_studentid = mysqli_real_escape_string($connection, $_SESSION['id']);
	$docname =  mysqli_real_escape_string($connection,$files['inputstudentactivity7']['name']);
	$filename =  mysqli_real_escape_string($connection,basename($files['inputstudentactivity7']['tmp_name']));

	$sql_delete = "
		delete from activities_answer 
		where fk_activity = '$fk_activity'
		and fk_studentid = '$fk_studentid';
	";

	$sql_insert = "
		INSERT INTO activities_answer 
		(fk_activity, fk_studentid, docname, filename) 
		VALUES ('$fk_activity', '$fk_studentid', '$docname', '$filename');
	";
	$delete = $connection->query($sql_delete);
	$insert = $connection->query($sql_insert);

	if ( $insert === TRUE and $delete === TRUE) {
	    return 'success';
	} else {
	    return "Error: " . $sql . "<br>" . $connection->error;
	}
}
function deleteActivity($connection,$data){
	$id = $data['id'];
	$sql = "
		DELETE FROM activities WHERE id = '$id';
	";

	if ($connection->query($sql) === TRUE) {
	    echo 'success';
	} else {
	    echo "Error: " . $sql . "<br>" . $connection->error;
	}
}
function getActivity($connection){
	$useryear = $_SESSION['useryear'];
	$usersection = $_SESSION['usersection'];

	//visible to student
	if ($_SESSION['levelID'] == 1) {
		$query = " 
			SELECT 
				activities.id as id,
				activities.activity_date as activity_date,
				activities.activity_to_year as activity_to_year,
				activities.activity_to_section as activity_to_section,
				activities.activity_by as activity_by,
				activities.content as content,
				activities.docname as docname, 
				activities.filename as filename,
				concat(tbl_user.firstname,' ',tbl_user.lastname) as teachername,
				activities_answer.id as isuploaded
				
			FROM activities
			left join tbl_user
				on activity_by = tbl_user.id
			left join activities_answer
				on activities_answer.fk_activity = activities.id
			where activity_to_year = '$useryear'
			and activity_to_section = '$usersection'
			and activities.status = 'open';";

	//visible to teacher
	}else{
		$query = " 
			SELECT 
				activities.id as id,
				activities.activity_date as activity_date,
				activities.activity_to_year as activity_to_year,
				activities.activity_to_section as activity_to_section,
				activities.content as content,
				user_section.label as sectionname,
				user_grade.label as gradename,
				docname, 
				filename,
				activities.content as content
			FROM activities 
			left join user_section
			ON  activity_to_section = user_section.id 

			left JOIN user_grade
			on activities.activity_to_year = user_grade.id

			where activity_by = ".$_SESSION['userID'];
	}

    $data = [];
    $result = mysqli_query($connection,$query);
    while($row = $result->fetch_assoc()) {
        $data[]=$row;
    }
    return $data;
}
function addactivity($connection,$data){
	$activityUserYear = mysqli_real_escape_string($connection,$_POST['activityUserYear']);
	$activityUserSection = mysqli_real_escape_string($connection,$_POST['activityUserSection']);
	$activitydate = mysqli_real_escape_string($connection,$_POST['activitydate']);
	$activitycontent = mysqli_real_escape_string($connection,$_POST['activitycontent']);
	$uploadDocname = mysqli_real_escape_string($connection,$_POST['uploadDocname']);
	$uploadFilename = mysqli_real_escape_string($connection,$_POST['uploadFilename']);
	$activitystatus = mysqli_real_escape_string($connection,$_POST['activitystatus']);
	$id = $_SESSION['id'];
	$sql = "
		INSERT INTO activities (
		activity_date, 
		activity_to_year, 
		activity_to_section, 
		activity_by,
		content,
		docname,
		filename,
		status
		) VALUES (
		if('$activitydate'='',CURRENT_TIMESTAMP,'$activitydate'),
		'$activityUserYear',
		'$activityUserSection',
		'$id',
		'$activitycontent',
		'$uploadDocname',
		'$uploadFilename',
		'$activitystatus'
		)
	";

	if ($connection->query($sql) === TRUE) {
	    echo 'success';
	} else {
	    echo "Error: " . $sql . "<br>" . $connection->error;
	}
}
function saveTBISetting($connection, $data){
	
	$tbisy	 	 = mysqli_real_escape_string($connection,$_POST['tbisy']);
	$tbi_setting_student = (mysqli_real_escape_string($connection,$_POST['tbi_setting_student'])=='true'?1:0);
	$tbi_setting_teacher = (mysqli_real_escape_string($connection,$_POST['tbi_setting_teacher'])=='true'?1:0);

	//DEACTIVATE ALL TBI
	/*---START---*/
	$deactivate_all = "
		UPDATE tbi_setting 
			SET teacher_tbi = 0,
			student_tbi = 0
	";
	if ($connection->query($deactivate_all) !== TRUE) {
	    echo "Error: " . $sql . "<br>" . $connection->error;
	}
	/*---END---*/

	$sql_select = "
		select count(*) as count from tbi_setting where sy = '$tbisy';
	";
	$result = mysqli_query($connection,$sql_select);
	$result = $result->fetch_assoc();
	if ( (int)$result['count'] > 0 ) {
		$sql_update = "
			UPDATE tbi_setting 
				SET teacher_tbi = '$tbi_setting_student',
				student_tbi = '$tbi_setting_teacher'
				WHERE  sy = '$tbisy'
		";
		if ($connection->query($sql_update) !== TRUE) {
		    echo "Error: " . $sql . "<br>" . $connection->error;
		}else{
			echo 'success';
		}
	}else{
		$sql = "
			INSERT INTO tbi_setting (
				sy, teacher_tbi, student_tbi
				) VALUES (
				'$tbisy',
				'$tbi_setting_student',
				'$tbi_setting_teacher'
				);";

		if ($connection->query($sql) === TRUE) {
		    echo 'success';
		} else {
		    echo "Error: " . $sql . "<br>" . $connection->error;
		}
	}
}

function getUserTBISetting($connection,$where){
	$table= $where.'_tbi';
	$query = "
		SELECT * FROM tbi_setting WHERE $table = 1";
    $data = [];
    $result = mysqli_query($connection,$query);
    while($row = $result->fetch_assoc()) {
        $data[]=$row;
    }
    return $data;
}

function getTBISetting($connection){
	$query = " 
		SELECT * FROM tbi_setting WHERE teacher_tbi = 1 or student_tbi = 1";
    $data = [];
    $result = mysqli_query($connection,$query);
    while($row = $result->fetch_assoc()) {
        $data[]=$row;
    }
    return $data;
}
function gettbiAnswerList($connection,$where){
	$query = " 
		SELECT * FROM tbi_answer_list
		where visible_to = '$where'";
    $data = [];
    $result = mysqli_query($connection,$query);
    while($row = $result->fetch_assoc()) {
        $data[]=$row;
    }
    return $data;
}
function getTeachers($connection){
	$query = " 
		SELECT * FROM tbl_user where levelID = 4 or levelID = 5 or levelID = 3 or levelID = 6 or levelID = 2";
    $data = [];
    $result = mysqli_query($connection,$query);
    while($row = $result->fetch_assoc()) {
        $data[]=$row;
    }
    return $data;
}

function getTeachersForTBIListT($connection){
	$c_user = $_SESSION['id'];
	$query = " 
		SELECT 
			tbl_user.id as id,
			firstname, 
			lastname
		FROM tbl_user
        where id not in ( 
        	SELECT DISTINCT teacher_id FROM `tbi_answer` where student_id = '$c_user'
        )
        and levelID = 4
        and tbl_user.id != '$c_user'
		";
    $data = [];
    $result = mysqli_query($connection,$query);
    while($row = $result->fetch_assoc()) {
        $data[]=$row;
    }
    return $data;
}

function getTeachersForTBIList($connection){
	$c_user = $_SESSION['id'];
	$query = " 
		SELECT 
			tbl_user.id as id,
			firstname, 
			lastname
		FROM tbl_user
        where id not in ( 
        	SELECT DISTINCT teacher_id FROM `tbi_answer` where student_id = '$c_user'
        )
        and levelID = 4
		";
    $data = [];
    $result = mysqli_query($connection,$query);
    while($row = $result->fetch_assoc()) {
        $data[]=$row;
    }
    return $data;
}

function addTBI($connection,$data){
	$data = json_decode($data,true);

	$counter = 0;
	$user = $_SESSION['id'];
	foreach ($data as $key => $value) {
		$q_id =  mysqli_real_escape_string($connection, $value['id']);
		$answer =  mysqli_real_escape_string($connection, $value['answer']);
		$teacher =  mysqli_real_escape_string($connection, $value['teacher']);
		$sql = "
			INSERT INTO tbi_answer (
				student_id, 
				teacher_id, 
				tbi_question,
				user_answer
			) VALUES (
				'$user', '$teacher', '$q_id', '$answer');
			";
		if ($connection->query($sql) === TRUE) {
		    $counter++;
		} else {
		    echo "Error: " . $sql . "<br>" . $connection->error;
		}
	}
	$insert = "
		INSERT INTO tbi_status(
			tbi_type, 
			student_id, 
			tbi_status
		) VALUES (
			'student',
			'$user',
			'1'
		)
	";

	if ( count($data) == $counter) {
		if ($connection->query($insert) === TRUE) {
			echo 'success';
		} else {
		    echo "Error: " . $sql . "<br>" . $connection->error;
		}

	}else{
		var_dump($counter);
		var_dump( count($data));
	}
}

function getQuestions($connection,$where){
	$query = "
		SELECT * FROM tbi_questions where questions_for = '$where'";
    $data = [];
    $result = mysqli_query($connection,$query);
    while($row = $result->fetch_assoc()) {
        $data[]=$row;
    }
    return $data;
}

function getAnnouncement($connection){
	$userid = $_SESSION['id'];
	$where = "
		where tbl_user.id = '$userid'
	";
	$id =  mysqli_real_escape_string($connection, $_SESSION['id']);
	$usergrade = $_SESSION['useryear'];
	$usersection = $_SESSION['usersection'];

	//FOR STUDENTS ONLY
	if( $_SESSION['levelID'] == 1){
		$where = " where 
		user_grade.id = '$usergrade' 
		and user_section.id = '$usersection' ";
	}

	$query = " 
		SELECT
			tbl_user.id,
			tbl_user.firstname,
			tbl_user.lastname,
			date_format(announcement_date, '%Y-%m-%d') as announcement_date,
			announce_to_year, 
			announce_to_section,
			announce_by,
			content ,
			user_grade.label as usergrade,
			user_section.label as usersection,
			announcement.id as announcementid
		FROM announcement 
		left JOIN tbl_user 
			on tbl_user.id = announcement.announce_by
		left JOIN user_grade
			on user_grade.id = announcement.announce_to_year
		LEFT join user_section
			on user_section.id = announcement.announce_to_section
		" . $where;
    $data = [];
    $result = mysqli_query($connection,$query);
    while($row = $result->fetch_assoc()) {
        $data[]=$row;
    }
    return $data;
}

function delAnnouncement($connection){
	$id = mysqli_real_escape_string($connection,$_POST['id']);
	$sql = "
		delete from announcement where id  = '$id'
	";


	if ($connection->query($sql) === TRUE) {
	    echo 'success';
	} else {
	    echo "Error: " . $sql . "<br>" . $connection->error;
	}
}

function addAnnouncement($connection,$data){
	$announcementUserYear = mysqli_real_escape_string($connection,$_POST['announcementUserYear']);
	$announcementUserSection = mysqli_real_escape_string($connection,$_POST['announcementUserSection']);
	$announcementdate = mysqli_real_escape_string($connection,$_POST['announcementdate']);
	$announcementcontent = mysqli_real_escape_string($connection,$_POST['announcementcontent']);
	$id = $_SESSION['id'];
	$sql = "
		INSERT INTO announcement (
		announcement_date, 
		announce_to_year, 
		announce_to_section, 
		announce_by,
		content
		) VALUES (
		'$announcementdate',
		'$announcementUserYear',
		'$announcementUserSection',
		'$id',
		'$announcementcontent'
		)
	";


	if ($connection->query($sql) === TRUE) {
	    echo 'success';
	} else {
	    echo "Error: " . $sql . "<br>" . $connection->error;
	}
}

function saveGrade($connection,$data){
	$data = json_decode($data,true);

	//DO DELETE
	$counter = 0;
	foreach ($data as $key => $value) {
		$fk_student_id =  mysqli_real_escape_string($connection, $value['student']);
		$fk_subject_id =  mysqli_real_escape_string($connection, $value['subject']);
		$fk_teacher_id =  mysqli_real_escape_string($connection, $_SESSION['id']);
		$grading_1 =  mysqli_real_escape_string($connection, $value['g1']);
		$grading_2 =  mysqli_real_escape_string($connection, $value['g2']);
		$grading_3 =  mysqli_real_escape_string($connection, $value['g3']);
		$grading_4 =  mysqli_real_escape_string($connection, $value['g4']);

		//SELECT
		$sql_select = "
			SELECT count(*)
			FROM student_grade 
			WHERE 
			fk_student_id = '$fk_student_id'
			and fk_subject_id = '$fk_subject_id'
			and fk_teacher_id = '$fk_teacher_id'
			";
		$result = mysqli_query($connection,$sql_select);
		$currentRec = $result->fetch_row();
		if ($currentRec[0] == '0') {
			$sql = "
				INSERT INTO student_grade(
						fk_student_id, 
						fk_subject_id, 
						fk_teacher_id, 
						grading_1, 
						grading_2, 
						grading_3, 
						grading_4 ) 
					VALUES ( 
						'$fk_student_id',
						'$fk_subject_id',
						'$fk_teacher_id',
						'$grading_1',
						'$grading_2',
						'$grading_3',
						'$grading_4' );
				";
		}else{
			$sql = "
				update student_grade 
				set
					grading_1 = '$grading_1', 
					grading_2 = '$grading_2', 
					grading_3 = '$grading_3', 
					grading_4 = '$grading_4'
				where 
					fk_student_id = '$fk_student_id' and 
					fk_subject_id = '$fk_subject_id' and 
					fk_teacher_id = '$fk_teacher_id';
				";
		}


		if ($connection->query($sql) === TRUE) {
		    $counter++;
		} else {
		    echo "Error: " . $sql . "<br>" . $connection->error;
		}
	}
	if ( count($data) == $counter) {
		echo 'success';
	}else{
		var_dump($counter);
		var_dump( count($data));
	}
}

// Student Grade
function getStudents($connection,$where,$l){
	$subject  = mysqli_real_escape_string($connection, $_POST['dataSubject']);

	//IF STUDENT DONOT FILTER THE SUBJECT
	if ($_SESSION['levelID'] != 1) {
		$w1 = " and fk_subject_id = '$subject' ";
		$w2 = " and fk_subject_id is not null ";
		$studentFilter = ( isset($_POST['dataSubject']) ? $w1 : $w2 );
	}
	$query = " 
		SELECT  tbl_user.id,tbl_user.firstname,
		tbl_user.lastname,
		if(grading_1 is null,'No Grade',grading_1) as grading_1,
		if(grading_2 is null,'No Grade',grading_2) as grading_2,
		if(grading_3 is null,'No Grade',grading_3) as grading_3,
		if(grading_4 is null,'No Grade',grading_4) as grading_4
		FROM tbl_user 
		LEFT JOIN student_grade 
		on tbl_user.id = student_grade.fk_student_id
		".$studentFilter."
		where levelID=1
		and ".$where."
		limit ".$l;
	//echo $query;
    $data = [];
    $result = mysqli_query($connection,$query);
    while($row = $result->fetch_assoc()) {
        $data[]=$row;
    }
    return $data;
}

function getSubject($connection){
	$where = "";
	if (  array_key_exists('year',$_POST) ) {
		$year = $_POST['year'];
		$where .= " where fk_grade_id = '$year' ";
	} 
	$query = " 
		SELECT * FROM user_subject ". $where;
    $data = [];
    $result = mysqli_query($connection,$query);
    while($row = $result->fetch_assoc()) {
        $data[]=$row;
    }
    return $data;
}

function getSections($connection){
	$query = " 
		SELECT * FROM user_section ";
    $data = [];
    $result = mysqli_query($connection,$query);
    while($row = $result->fetch_assoc()) {
        $data[]=$row;
    }
    return $data;
}

//GET SCHED OF STUDENT BASE ON GRADE
function getSchedOfStudent($connection){
	$user = $_SESSION['id'];
	$useryear = $_SESSION['useryear'] ;
	$usersection = $_SESSION['usersection'] ;
	$query = " 
		SELECT 
			user_subject.label as subjectname,
			concat (tbl_user.firstname,' ',tbl_user.lastname) as teachername,
			start_time,
			end_time
		FROM `schedule`
		left join user_grade
			on schedule.subject_id = user_grade.id
		left join tbl_user
			on tbl_user.id = schedule.teacher_id
		left join user_subject
			on user_subject.id = subject_id
		where section_id = '$usersection'
		and grade_id = '$useryear'
		";
    $data = [];
    $result = mysqli_query($connection,
    	$query);
    while($row = $result->fetch_assoc()) {
        $data[]=$row;
    }
    return $data;
}

// FOR TEACHER , LIST OF SCHED BY REGISTRAR
function getSchedOfTeacher($connection){
	$user = $_SESSION['id'];
	$query = " 
		SELECT
			schedule.id as id,
			user_section.label as sectionname,
			user_grade.label as gradename,
			user_subject.label as subjectname,
			start_time,
			end_time
		FROM schedule
		left join user_section
		on user_section.id = schedule.section_id

		left join user_grade
		on user_grade.id = schedule.grade_id

		left join user_subject
		on user_subject.id  = schedule.subject_id

		where teacher_id = '$user'
		";
    $data = [];
    $result = mysqli_query($connection,
    	$query);
    while($row = $result->fetch_assoc()) {
        $data[]=$row;
    }
    return $data;
}

//FOR REGISTRAR TABLE OF SCHEDULE LIST
function getSchedule($connection){
	$t = $_POST['t'];
	$where = "";
	if ( $t != '0') {
		$where = " where schedule.teacher_id = '$t' ";
	}
	
	$query = " 
		SELECT schedule.id as ID,
		start_time as starttime ,
		end_time as endtime,
		subject_id as subject,
		grade_id as gradeid,
		section_id as sectionid,
		user_subject.label as subjectname,
		user_grade.label as gradename,
		user_section.label as sectionname
		FROM schedule
		left join user_subject
		on schedule.subject_id = user_subject.id
		left join user_grade
		on user_grade.id = schedule.grade_id
		left join user_section
		on user_section.id = schedule.section_id
		" . $where;
    $data = [];
    $result = mysqli_query($connection,
    	$query);
    while($row = $result->fetch_assoc()) {
    	//var_dump($row['ID']);
    	$row['ID'] = (int) $row['ID'];
        $data[]=$row;
    }
    //var_dump($data);
    $data = json_encode( $data );
    return $data;
}

function deleteSchedule($connection){
	$id = mysqli_real_escape_string($connection,$_POST['id']);
	
	$sql = "
		delete from schedule where id = '$id';
	";

	if ($connection->query($sql) === TRUE) {
	    $json = array();
	    array_push($json, array('status'=>'success', 'id'=> $id));
	    echo json_encode($json);
	} else {
	    echo "Error: " . $sql . "<br>" . $connection->error;
	}
}

function updateSchedule($connection){
	$id = mysqli_real_escape_string($connection,$_POST['id']);
	$teacher_id = mysqli_real_escape_string($connection,$_POST['teacherid']);
	$grade_id = mysqli_real_escape_string($connection,$_POST['gradeid']);
	$section_id = mysqli_real_escape_string($connection,$_POST['sectionid']);
	$subject_id = mysqli_real_escape_string($connection,$_POST['subjectid']);
	$start_time = mysqli_real_escape_string($connection,$_POST['starttime']);
	$end_time = mysqli_real_escape_string($connection,$_POST['endtime']);
	
	$sql = "
	update schedule 
	set teacher_id =  '$teacher_id', 
	grade_id = '$grade_id',
	section_id = '$section_id',
	subject_id = '$subject_id', 
	start_time = '$start_time', 
	end_time = '$end_time'
	where id = '$id'
	";

	if ($connection->query($sql) === TRUE) {
	    $json = array();
	    array_push($json, array('status'=>'success', 'id'=> $id));
	    echo json_encode($json);
	} else {
	    echo "Error: " . $sql . "<br>" . $connection->error;
	}
}

function addSchedule($connection){

	$teacher_id = mysqli_real_escape_string($connection,$_POST['teacherid']);
	$grade_id = mysqli_real_escape_string($connection,$_POST['gradeid']);
	$section_id = mysqli_real_escape_string($connection,$_POST['sectionid']);
	$subject_id = mysqli_real_escape_string($connection,$_POST['subjectid']);
	$start_time = mysqli_real_escape_string($connection,$_POST['starttime']);
	$end_time = mysqli_real_escape_string($connection,$_POST['endtime']);
	
	$sql = "
	INSERT INTO schedule ( teacher_id, grade_id, section_id, subject_id, start_time, end_time
	) VALUES ( '$teacher_id', '$grade_id', '$section_id', '$subject_id', '$start_time', '$end_time'  );";

	if ($connection->query($sql) === TRUE) {
	    $id = $connection->insert_id;
	    $json = array();
	    array_push($json, array('status'=>'success', 'id'=> $id));
	    echo json_encode($json);
	} else {
	    echo "Error: " . $sql . "<br>" . $connection->error;
	}
}


function getSubjectByGradeSched($connection){
	$gradeid = mysqli_real_escape_string($connection, $_POST['gradeid']);
	$c_user = $_SESSION['id'];
	$query = " 
		SELECT  
			user_subject.id , 
			user_subject.label  
		FROM schedule 
		LEFT join user_subject 
		on user_subject.id = schedule.subject_id
		where teacher_id = '$c_user'
		and grade_id = '$gradeid'
		";
    $data = [];
    $result = mysqli_query($connection,$query);
    while($row = $result->fetch_assoc()) {
        $data[]=$row;
    }
    return $data;
}

function getSubjectByGrade($connection){
	$gradeid = mysqli_real_escape_string($connection, $_POST['gradeid']);
	$query = " 
		SELECT * FROM user_subject 
		where fk_grade_id = '$gradeid' ";
    $data = [];
    $result = mysqli_query($connection,$query);
    while($row = $result->fetch_assoc()) {
        $data[]=$row;
    }
    return $data;
}

//Grade by Sched
function schedGrade($connection){
	$c_user = $_SESSION['id'];
	$query = " 
		SELECT 
			DISTINCT grade_id as id, 
			user_grade.label
		FROM `schedule`
		LEFT join user_grade
		on user_grade.id = schedule.grade_id
		where teacher_id = '$c_user'
		";
    $data = [];
    $result = mysqli_query($connection,$query);
    while($row = $result->fetch_assoc()) {
        $data[]=$row;
    }
    return $data;
}

//Section by sched
function schedSection($connection){
	$c_user = $_SESSION['id'];
	$query = " 
		SELECT 
			DISTINCT grade_id as id, 
			user_grade.label
		FROM `schedule`
		LEFT join user_grade
		on user_grade.id = schedule.grade_id
		where teacher_id = '$c_user'
		";
    $data = [];
    $result = mysqli_query($connection,$query);
    while($row = $result->fetch_assoc()) {
        $data[]=$row;
    }
    return $data;
}

function getSectionsByGradeSched($connection){
	$gradeid = mysqli_real_escape_string($connection, $_POST['gradeid']);
	$c_user = $_SESSION['id'];
	$query = "
		SELECT 
			id,
			label 
		FROM user_section 
		where id in (
			SELECT 
				DISTINCT section_id 
				FROM schedule  
				where teacher_id = '$c_user'
		)
		and fk_grade_id = '$gradeid'
	";
    $data = [];
    $result = mysqli_query($connection,$query);
    while($row = $result->fetch_assoc()) {
        $data[]=$row;
    }
    return $data;
}

function getSectionsByGrade($connection){
	$gradeid = mysqli_real_escape_string($connection, $_POST['gradeid']);
	$query = " 
		SELECT * FROM user_section 
		where fk_grade_id = '$gradeid' ";
    $data = [];
    $result = mysqli_query($connection,$query);
    while($row = $result->fetch_assoc()) {
        $data[]=$row;
    }
    return $data;
}

function getGradeLevel($connection){
	$query = " 
		SELECT * FROM user_grade ";
    $data = [];
    $result = mysqli_query($connection,$query);
    while($row = $result->fetch_assoc()) {
        $data[]=$row;
    }
    return $data;
}

function getUserType($connection,$where){
    $query = "select * from account_type where ". $where;
    $data = [];
    $result = mysqli_query($connection,$query);
    while($row = $result->fetch_assoc()) {
        $data[]=$row;
    }
    return $data;
}

function addUser($connection,$data){

	$username	 = mysqli_real_escape_string($connection,$_POST['username']);
	$firstname	 = mysqli_real_escape_string($connection,$_POST['firstname']);
	$lastname	 = mysqli_real_escape_string($connection,$_POST['lastname']);
	$email	 	 = mysqli_real_escape_string($connection,$_POST['email']);
	$password	 = mysqli_real_escape_string($connection,$_POST['password']);
	$usertype	 = mysqli_real_escape_string($connection,$_POST['usertype']);

	$useryear	 = mysqli_real_escape_string($connection,$_POST['useryear']);
	$usersection = mysqli_real_escape_string($connection,$_POST['usersection']);

	$sql = "
		INSERT INTO tbl_user (
			username, firstname, lastname, email, password, levelID,useryear,usersection
		) VALUES(
			'$username', '$firstname', '$lastname', '$email', 
			'$password',$usertype,'$useryear','$usersection'
		)";


	if ($connection->query($sql) === TRUE) {
	    echo 'success';
	} else {
	    echo "Error: " . $sql . "<br>" . $connection->error;
	}
}

function deleteUser($connection,$id){
	$id	 = mysqli_real_escape_string($connection,$_GET['id']);
	$sql = "delete from tbl_user where id = '$id'";
	if ($connection->query($sql) === TRUE) {
	    return 'success';
	} else {
	    return "Error: " . $sql . "<br>" . $connection->error;
	}
}

function editUser($connection,$data){
	$username = mysqli_real_escape_string($connection,$data["username"]);
	$firstname = mysqli_real_escape_string($connection,$data["firstname"]);
	$lastname = mysqli_real_escape_string($connection,$data["lastname"]);
	$email = mysqli_real_escape_string($connection,$data["email"]);
	$password = mysqli_real_escape_string($connection,$data["password"]);
	$usertype = mysqli_real_escape_string($connection,$data["usertype"]);
	$id = mysqli_real_escape_string($connection,$data["id"]);
	$data = "username='$username',firstname='$firstname',lastname='$lastname',email='$email',password='$password',levelID='$usertype'";
	$sql = "update tbl_user set ".$data. " where id=".$id;
	if ($connection->query($sql) === TRUE) {
	    return 'success';
	} else {
	    return "Error: " . $sql . "<br>" . $connection->error;
	}
}

function addlecture($connection,$post){
	$lectureUserYear = mysqli_real_escape_string($connection,$post["lectureUserYear"]);
	$lectureUserSection = mysqli_real_escape_string($connection,$post["lectureUserSection"]);
	$lecturedate = mysqli_real_escape_string($connection,$post["lecturedate"]);
	$lecturecontent = mysqli_real_escape_string($connection,$post["lecturecontent"]);
	$uploadDocnameLecture = mysqli_real_escape_string($connection,$post['uploadDocnameLecture']);
	$uploadFilenameLecture = mysqli_real_escape_string($connection,$post['uploadFilenameLecture']);
	$lectureSubject = mysqli_real_escape_string($connection,$post['lectureSubject']);
	$currentUser = $_SESSION['id'];

	$sql = " 
		INSERT INTO lectures (saved_ts, yearid, sectionid, subjectid, title, added_by, docname, filename) 
		VALUES ('$lecturedate', '$lectureUserYear', '$lectureUserSection', '$lectureSubject', '$lecturecontent',
		 '$currentUser', '$uploadDocnameLecture', '$uploadFilenameLecture');
	";
	if ($connection->query($sql) === TRUE) {
	    return 'success';
	} else {
	    return "Error: " . $sql . "<br>" . $connection->error;
	}
}

function deleteLecture($connection,$post){
	$id = mysqli_real_escape_string($connection,$post['id']);
	$sql = "
		delete from lectures where id = '$id';
	";
	if ($connection->query($sql) === TRUE) {
	    return 'success';
	} else {
	    return "Error: " . $sql . "<br>" . $connection->error;
	}

}

?>
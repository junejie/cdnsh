<?php 
require_once('Connections/conn.php');
include('inc_security.php');
include('app/proc/permission.php');

?>

<!doctype html>
<html>
<head>
    <meta charset="utf-8">
	<title><?php include('assets/includes/inc_title.php');?></title>
	<meta name="description" content="File Upload widget with multiple file selection, drag&amp;drop support, progress bars, validation and preview images, audio and video for jQuery. Supports cross-domain, chunked and resumable file uploads and client-side image resizing. Works with any server-side platform (PHP, Python, Ruby on Rails, Java, Node.js, Go etc.) that supports standard HTML form file uploads.">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

   	<!-- Bootstrap CSS -->
  	<link rel="stylesheet" href="assets/css/bootstrap.css">
  	<link rel="stylesheet" href="assets/css/bootstrap-theme.css">


  	<!-- jquery -->
  	<script src="assets/js/jquery.js"></script>
  	<script src="assets/js/bootstrap.js"></script> 
  	<script src="assets/js/jqueryui/jquery-ui.min.js"></script>
    <link rel='stylesheet' href='assets/js/jqueryui/themes/smoothness/jquery-ui.min.css' />
    <link rel='stylesheet' href='assets/js/jqueryui/themes/smoothness/theme.css' />


	<!-- FAVICON ICON-->
    <link rel="shortcut icon" href="images/favicon.ico"/>

    <!-- calendar plugin -->
    <link rel='stylesheet' href='assets/js/plugins/fullcalendar/fullcalendar.css' />
	<script src='assets/js/moment.min.js'></script>
	<script src='assets/js/plugins/fullcalendar/fullcalendar.js'></script>
	
	<?php 
		echo permissions('js-calendar-view');
		echo permissions('js-calendar');
	?>

	<link rel="stylesheet" href="assets/css/main.css">
</head>
<body>
<div class="gridContainer clear">
<?php include("assets/includes/inc_navigation.php");?>
    <div id="content">
        <div id="contentpad">
			<?php
				echo permissions('banner');
				echo permissions('calendar-admin');
				echo permissions('calendar-view');
			?>	
		</div>
    </div>

      <!-- calendar event -->
	<div id="eventContent" title="Event Details" style="display:none;">
	    <span id="eventID"></span><br><br>
	</div>

	<!-- calendar add -->
	<div id="dialog" style="display: none;">
		<div class="form-group">
			
			<label for="c_title">Title</label>
			<input class="form-control" type="text" id="c_title" />
		</div>
		<div class="form-group">
			<label for="c_content">Content</label>
			<textarea class="form-control" id="c_content"></textarea>
		</div>
	</div>

    </div>
</div>

</body>
</html>

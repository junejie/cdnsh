<?php 
	require_once('Connections/conn.php');
	include('inc_security.php');
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<title><?php include('assets/includes/inc_title.php');?></title>

   	<!-- Bootstrap CSS -->
  	<link rel="stylesheet" href="assets/css/bootstrap.css">
  	<!-- Bootstrap JS -->
	<script src="assets/js/jquery.js"></script>
	<script src="assets/js/bootstrap.js"></script>  

  	<script>
		$(document).ready(function() {
			$(function(){
				$('.pix_diapo').diapo();
			});
		});
	</script>	
	<noscript>
		<style>
			.da-thumbs li a div {
				top: 0px;
				left: -100%;
				-webkit-transition: all 0.3s ease;
				-moz-transition: all 0.3s ease-in-out;
				-o-transition: all 0.3s ease-in-out;
				-ms-transition: all 0.3s ease-in-out;
				transition: all 0.3s ease-in-out;
			}
			.da-thumbs li a:hover div{
				left: 0px;
			}
		</style>
	</noscript>	

  	<link rel="stylesheet" href="assets/css/DT_bootstrap.css">
  	<link rel="stylesheet" href="assets/css/print.css">
	<!-- FAVICON ICON-->
    <link rel="shortcut icon" href="images/favicon.ico"/>

	<style>
		body{
			background: url(assets/images/grid_noise.png);
    		position: relative;
		}
		.carousel-inner > .item > img, .carousel-inner > .item > a > img {
			width: 100%;
			margin: auto;
		}
	</style>

	

	<script>
		$(document).ready(function(){
			$('#product-table a:first').tab('show');
		});
	</script>

	<script type="text/javascript" charset="utf-8" language="javascript" src="assets/js/jquery.dataTables.js"></script>
    <script type="text/javascript" charset="utf-8" language="javascript" src="assets/js/DT_bootstrap.js"></script>

</head>
<body>
	<div class="gridContainer clear">
		<?php include("assets/includes/inc_navigation.php");?>
		<div class="container">
			<div class="col-lg-12"> 
			    <div class="bs-component">
			        <ul class="nav nav-tabs responsive">
			            <li class="active">
			                <a data-toggle="tab" href="#tab1">Sections</a>
			            </li>
			            <li>    
			                <a data-toggle="tab" href="#tab2">Grades Tab</a>
			            </li>
			            <li>
			                <a data-toggle="tab" href="#tab3">Activity Tab</a>
			            </li>
			            <li>
			                <a data-toggle="tab" href="#tab4">Announcement tab</a>
			            </li>
			            <li>
			                <a data-toggle="tab" href="#tab5">Schedule Daily</a>
			            </li>	
			            <li>
			                <a data-toggle="tab" href="#tab6">TEACHER AVALUATION</a>
			            </li>
			            <li>
			                <a data-toggle="tab" href="#tab7">TEACHER ACCOUNTS</a>
			            </li>
			         </ul>

			        <div class="tab-content">
			            <div id="tab1" class="tab-pane fade active in">
			                <table cellpadding="0" cellspacing="0" border="0" class="table  table-bordered" id="example">
			                    <div class="alert alert-info">
			                        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">Add Subject</button>	 
								    <!-- Modal add user -->
									<?php include('modal_add_course.php');?>
			                    </div>
			                    <thead>
			                    	<tr>
										<th>Grade</th>
										<th>Section Title</th>
										<th>Action</th>
									</tr>
			                    </thead>

			                    <tbody>
								 
									<?php $query = "select * from tbl_sections where isdelete = 1";
									$result = mysqli_query($conn,$query);
									while($row = mysqli_fetch_assoc($result)){
									$id=$row['section_id']; ?>
									<tr>
										<td><?php echo $row['code-section']; ?></td>
										<td><?php echo $row['title-section']; ?></td>
										<td>
											<!-- edit subject -->
											<button type="button" class="btn btn-info" data-toggle="modal" data-target="#editModal"><span class="glyphicon glyphicon-edit"></span> Edit Section</button>	 
											<?php include('edit-section-modal.php');?>

											<!-- delete subject -->
											<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal"><span class="glyphicon glyphicon-trash"></span> Delete Section</button>	 
											<?php include('delete_subject.php');?>
										</td>
									</tr>
									<?php } ?>
			               
			                    </tbody>
			                </table>
			             </div>
			             <div id="tab2" class="tab-pane fade active">
			                <table class="table table-condensed table-bordered table-striped volumes">
								<thead>
									<tr>
									<th>Warehouse</th>
									<th>Grain Volume</th>
									<th>Trade Volume</th>
									<th>Direction</th>
									</tr>
								</thead>
								
								<tbody>
									<tr>
										<td>National Food Reserve Agency_Manyoni</td>
										<td>172</td>
										<td>172.00</td>
										<td>IN</td>
									</tr>
									<tr>
										<td>Sodea GBC</td>
										<td>471</td>
										<td>20.00</td>
										<td>OUT</td>
									</tr>
								</tbody>
							</table>
			             </div>
			             <div id="tab3" class="tab-pane fade active">
				        	<h4>ACTIVITY!</h4>
							<table class="table table-condensed table-bordered table-striped volumes">
								<thead>
									<tr>
										<th>Date</th>
										<th>Where</th>
										<th>What</th>
										<th>Time</th>
									</tr>
								</thead>
								
								<tbody>
									<tr>
										<td>dec 23, 2015</td>
										<td>School Grounds</td>
										<td>Christmas Party</td>
										<td>6:00 A.M - 3:00 P.M</td>
									</tr>
									<tr>
										<td>jan 1, 2015</td>
										<td>School Grounds</td>
										<td>Fireworks Display</td>
										<td>5:00 A.M - 8:00 P.M</td>
									</tr>
								</tbody>
							</table>
				        </div>
				        <div id="tab4" class="tab-pane fade active">
				        	<h3>ANNOUNCEMENT!</h3>
							<article>WALANG PASOK!</article>
				        </div>
				        <div id="tab5" class="tab-pane fade active">
				        	<h3>Daily Schedule</h3>
				        	<table class="table table-condensed table-bordered table-striped volumes">
								<thead>
									<tr>
										<th>Mon</th>
										<th>Tue</th>
										<th>Wed</th>
										<th>Thurs</th>
										<th>Fri</th>
									</tr>
								</thead>
								
								<tbody>
									<tr>
										<td>6:00 A.M - 3:00 P.M</td>
										<td>12:00 P.M  - 3:00 P.M</td>
										<td>6:00 A.M - 3:00 P.M</td>
										<td>12:00 P.M  - 3:00 P.M</td>
										<td>6:00 A.M - 3:00 P.M</td>
									</tr>
									<tr>
										<td>1:00 P.M  - 3:00 P.M</td>
										<td>4:00 P.M  - 6:00 P.M</td>
										<td>1:00 P.M  - 3:00 P.M</td>
										<td>4:00 P.M  - 6:00 P.M</td>
										<td>1:00 P.M  - 3:00 P.M</td>
									</tr>
								</tbody>
							</table>
				        </div>
				        <div id="tab6" class="tab-pane fade active">
				        	<h4>TEACHER AVALUATION</h4>
				        	<h5> Comming Soon...</h5>
				        </div>
				        <div id="tab7" class="tab-pane fade active">
				        	<h4>TEACHER ACCOUNTS</h4>
				        	<h5> Comming Soon...</h5>
				        </div>
			        </div>
			    </div>
			</div>
		</div>
</body>
</html>

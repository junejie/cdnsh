<?php 
	require_once('Connections/conn.php');
	include_once('app/proc/User.php');
	include('inc_security.php');
	require_once('app/lib/js-lib.php');

	$connection = db_connect();
	if ($_SESSION['levelID']=='5') {
		$where = " group_user = 'student' ";
	} else {
		$where = '0';
	}
	
	$userlist = getUserList($connection,$where);
	$grade = getGradeLevel($connection);
	$section = getSections($connection);
	$subject = getSubject($connection);

	$user = $_SESSION['id'];
	$students = getStudents($connection," tbl_user.id = '$user' ",10);
	$announcementList = getAnnouncement($connection);
	$activityList = getActivity($connection);

	$questions = getQuestions($connection,'student');
	$teachers = getTeachers($connection);
	$getTeachersForTBIList = getTeachersForTBIList($connection);
	$tbiAnswerList = gettbiAnswerList($connection,'student');
	$tbiSetting = getUserTBISetting($connection,'student');
	$lectureList = getAssignedLecture($connection);

	//GET SCHED VALUE
	$sched = getSchedOfStudent($connection);
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<title><?php include('assets/includes/inc_title.php');?></title>

   	<!-- Bootstrap CSS -->
  	<link rel="stylesheet" href="assets/css/bootstrap.css">
  	<!-- Bootstrap JS -->
	<script src="assets/js/jquery.js"></script>
	<script src="assets/js/bootstrap.js"></script>  	
	
    <!-- upload plugin -->
	<link href="assets/js/plugins/bootstrap-fileinput/css/fileinput.min.css" media="all" rel="stylesheet" type="text/css" />
	<script src="assets/js/plugins/bootstrap-fileinput/js/plugins/canvas-to-blob.min.js" type="text/javascript"></script>
	<script src="assets/js/plugins/bootstrap-fileinput/js/fileinput.min.js"></script>
	<script src="assets/js/plugins/bootstrap-fileinput/js/fileinput_locale_LANG.js"></script>

	<noscript>
		<style>
			div#tab2 {
			    padding-top: 10px;
			}
			.da-thumbs li a div {
				top: 0px;
				left: -100%;
				-webkit-transition: all 0.3s ease;
				-moz-transition: all 0.3s ease-in-out;
				-o-transition: all 0.3s ease-in-out;
				-ms-transition: all 0.3s ease-in-out;
				transition: all 0.3s ease-in-out;
			}
			.da-thumbs li a:hover div{
				left: 0px;
			}
		</style>
	</noscript>	

  	<link rel="stylesheet" href="assets/css/DT_bootstrap.css">
  	<link rel="stylesheet" href="assets/css/print.css">
  	<link rel="stylesheet" href="assets/css/tab.css">
  	<link rel="stylesheet" href="assets/css/custom-1.css">
	<!-- FAVICON ICON-->
    <link rel="shortcut icon" href="images/favicon.ico"/>
	<script>
		$(document).ready(function(){
			$('#product-table a:first').tab('show');
		});
	</script>
	<script type="text/javascript" charset="utf-8" language="javascript" src="assets/js/jquery.dataTables.js"></script>
    <script type="text/javascript" charset="utf-8" language="javascript" src="assets/js/DT_bootstrap.js"></script>
</head>
<body>
	<div class="alert alert-success hidden">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<strong>Success!</strong>
	</div>
	<div class="gridContainer clear">
		<?php include("assets/includes/inc_navigation.php");?>
		<div class="container">
			<div class="col-lg-12"> 
			    <div class="bs-component">
			        <ul class="nav nav-tabs responsive">
			            <li class="active">
			                <a data-toggle="tab" href="#tab1">Activity</a>
			            </li>
			            <li>
			                <a data-toggle="tab" href="#tab2">Lecture</a>
			            </li>
			            <li>
			                <a data-toggle="tab" href="#tab3">Grade</a>
			            </li>	
			            <li>
			                <a data-toggle="tab" href="#tab4">Announcement</a>
			            </li>
			            <li>
			                <a data-toggle="tab" href="#sched-tab">Schedule</a>
			            </li>
			            <li>
			                <a data-toggle="tab" href="#tab5">TBI</a>
			            </li>
			         </ul>

			        <div class="tab-content">
			            <div id="tab1" class="tab-pane fade active in">
							<?php 
							include('app/form/activity-studenttab.php');
							?>
				        </div>
				        <div id="tab2" class="tab-pane fade active">
							<?php 
							include('app/form/lectures.php');
							?>
				        </div>
				        <div id="tab3" class="tab-pane fade active">
				        	<?php include('app/form/grade-student-tab.php');?>
				        </div>
				        <div id="tab4" class="tab-pane fade active">
				        	<?php include('app/form/announcement-tab.php');?>
				        </div>
				        <div id="sched-tab" class="tab-pane fade active">
				        	<?php include('app/form/schedule-student-view.php');?>
				        </div>
				        <div id="tab5" class="tab-pane fade active">
				        	<?php 
				        	if ( $tbiSetting ){
				        		include('app/form/tbi-for-students-tab.php');
				        	}else{
				        		include('app/form/info.php');
				        	}
				        	?>
				        </div>
			        </div>
			    </div>
			</div>
		</div>
	<!-- load js -->
<?php
	loadJS($_SESSION['levelID']);
?>
</body>
</html>

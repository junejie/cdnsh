<div id="deleteModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="submit" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Delete Section</h4>
			</div>
			<div class="modal-body">
				<div class="alert alert-danger">Are you Sure want you to Delete this Data?</div>
				<div class="modal-footer">
					<form name="form1" method="post" action="process.php" style="float:Right;">
						<input type="hidden" name="sID" id="hiddenField" value="<?php echo $row['section_id']; ?>">
			    		<button name="btnDeleteSection"  type="submit" class="btn btn-danger" id="btnDeleteSection"><span class="glyphicon glyphicon-trash"></span> Delete Section</button>
			    		<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
			    	</form>
				</div>
			</div>
		</div>

	</div>
</div>
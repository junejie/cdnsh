<?php 
	require_once('Connections/conn.php');
	include_once('app/proc/User.php');
	include('inc_security.php');
	require_once('app/lib/js-lib.php');

	$connection = db_connect();
	if ($_SESSION['levelID']=='5') {
		$where = " group_user = 'student' ";
	} else {
		$where = '0';
	}
	
	$userlist = getUserList($connection,$where);
	$grade = getGradeLevel($connection);
	$section = getSections($connection);
	$subject = getSubject($connection);
	$students = getStudents($connection,'true',10);
	$announcementList = getAnnouncement($connection);
	$lectureList = getlectureList($connection);
	$activityList = getActivity($connection);
	$teachers = getTeachers($connection);
	$gradelevel = getGradeLevel($connection);
	$tbiSetting = getUserTBISetting($connection,'teacher');
	$questions = getQuestions($connection,'teacher');
	$tbiAnswerList = gettbiAnswerList($connection,'teacher');
	$getTeachersForTBIListT = getTeachersForTBIListT($connection);
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<title><?php include('assets/includes/inc_title.php');?></title>

   	<!-- Bootstrap CSS -->
  	<link rel="stylesheet" href="assets/css/bootstrap.css">
  	<!-- Bootstrap JS -->
	<script src="assets/js/jquery.js"></script>
	<script src="assets/js/bootstrap.js"></script>  	
	<noscript>
		<style>
			div#tab2 {
			    padding-top: 10px;
			}
			.da-thumbs li a div {
				top: 0px;
				left: -100%;
				-webkit-transition: all 0.3s ease;
				-moz-transition: all 0.3s ease-in-out;
				-o-transition: all 0.3s ease-in-out;
				-ms-transition: all 0.3s ease-in-out;
				transition: all 0.3s ease-in-out;
			}
			.da-thumbs li a:hover div{
				left: 0px;
			}
		</style>
	</noscript>	

  	<link rel="stylesheet" href="assets/css/DT_bootstrap.css">
  	<link rel="stylesheet" href="assets/css/print.css">
  	<link rel="stylesheet" href="assets/css/tab.css">
	<!-- FAVICON ICON-->
    <link rel="shortcut icon" href="images/favicon.ico"/>

    <!-- upload plugin -->
	<link href="assets/js/plugins/bootstrap-fileinput/css/fileinput.min.css" 
	media="all" rel="stylesheet" type="text/css" />
	<link rel="stylesheet" href="assets/css/custom-5.css">
	<script src="assets/js/plugins/bootstrap-fileinput/js/plugins/canvas-to-blob.min.js"></script>
	<script src="assets/js/plugins/bootstrap-fileinput/js/fileinput.min.js"></script>
	<script src="assets/js/plugins/bootstrap-fileinput/js/fileinput_locale_LANG.js"></script>

	<!-- grid js plugin -->
	<script type="text/javascript" src="assets/js/jqueryui/jquery-ui.min.js"></script>
	<script type="text/javascript" src="assets/js/plugins/grid/grid-0.5.6.min.js"></script>
	<link rel="stylesheet" href="assets/js/plugins/grid/grid-0.5.6.css">
	<link rel="stylesheet" href="assets/js/jqueryui/themes/smoothness/jquery-ui.min.css">
	<link rel="stylesheet" href="assets/js/jqueryui/themes/smoothness/theme.css">

	<!-- data picker -->
	<script type="text/javascript" src="assets/js/plugins/timepicker/jquery.timepicker.min.js"></script>
	<link rel="stylesheet" href="assets/js/plugins/timepicker/jquery.timepicker.css">
	

	<script>
		$(document).ready(function(){
			$('#product-table a:first').tab('show');
		});
	</script>

	<script type="text/javascript" charset="utf-8" language="javascript" src="assets/js/jquery.dataTables.js"></script>
    <script type="text/javascript" charset="utf-8" language="javascript" src="assets/js/DT_bootstrap.js"></script>

</head>
<body>
	<div class="alert alert-success hidden">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<strong>Success!</strong>
	</div>
	<div class="gridContainer clear">
		<?php include("assets/includes/inc_navigation.php");?>
		<div class="container">
			<div class="col-lg-12"> 
			    <div class="bs-component">
			        <ul class="nav nav-tabs responsive">
			            <li class="active">
			                <a data-toggle="tab" href="#tab1">Accounts</a>
			            </li>
			            <li>    
			                <a data-toggle="tab" href="#grade-tab">Grades</a>
			            </li>
			            <li>
			                <a data-toggle="tab" href="#tab3">Activity</a>
			            </li>
			            <li>
			                <a data-toggle="tab" href="#tab4">Announcement</a>
			            </li>
			            <li>
			                <a data-toggle="tab" href="#tab5">Schedule</a>
			            </li>
			            <li>
			                <a data-toggle="tab" href="#tab6">Lectures</a>
			            </li>
			            <li>
			                <a data-toggle="tab" href="#tab7">OTBI</a>
			            </li>
			         </ul>

			        <div class="tab-content">
			            <div id="tab1" class="tab-pane fade active in">
			                <table cellpadding="0" cellspacing="0" border="0" class="table  table-bordered" id="example">
			                    <div class="alert alert-info">
			                        <button type="button" class="btn btn-info" data-toggle="modal" 
			                        data-target="#myModal">Add User</button>
								    <!-- Modal add user -->
									<?php include('app/modal/add-user.php');?>
			                    </div>
			                    <thead>
			                    	<tr>
										<th>First Name</th>
										<th>Last Name</th>
										<th>Email</th>
										<th>User Name</th>
										<th>User Type</th>
										<th>Actions</th>
									</tr>
			                    </thead>
			                    <tbody>
									<?php
									foreach ($userlist as $key => $row) {
									?>
									<tr>
										<td><?php echo $row['firstname']; ?></td>
										<td><?php echo $row['lastname']; ?></td>
										<td><?php echo $row['email']; ?></td>
										<td><?php echo $row['username']; ?></td>
										<td><?php echo $row['user_type']; ?></td>
										<td>
											<!-- edit user -->
											<button type="button" class="btn btn-info btn-xs" 
											data-toggle="modal" 
											data-target="#editModal<?=$row['id'];?>">
											<span class="glyphicon glyphicon-edit">
											</span> Edit User</button>	 
											<?php include('app/modal/edit-user.php');?>

											<!-- delete user -->
											<button type="button" class="btn btn-danger btn-xs" 
											data-toggle="modal" 
											data-target="#deleteModal<?=$row['id'];?>">
											<span class="glyphicon glyphicon-trash">
											</span> Delete User</button>	 
											<?php include('app/modal/delete-user.php');?>
										</td>
									</tr>
			               			<?php } ?>
			                    </tbody>
			                </table>
			            </div>
			            <div id="grade-tab" class="tab-pane fade active">
							<?php include('app/form/grade-filter.php');?>
							<?php include('app/form/grade-table.php');?>
			            </div>
			            <div id="tab3" class="tab-pane fade active">
				        	<?php include('app/form/activity-tab.php');?>
				        	<?php include('app/modal/delete-activity.php');?>
				        </div>
				        <div id="tab4" class="tab-pane fade active">
				        	<?php include('app/form/announcement-tab.php');?>
				        </div>
				        <div id="tab5" class="tab-pane fade active">
				        	<?php include('app/form/schedule.php');?> 
				        </div>
			            <div id="tab6" class="tab-pane fade active">
				        	<?php include('app/form/lectures-tab.php');?> 
			            </div>
			            <div id="tab7" class="tab-pane fade active">
				        	<?php 
				        	if ( $tbiSetting ){
				        		include('app/form/tbi-for-teacher-tab.php');
				        	}else{
				        		include('app/form/info.php');
				        	}
				        	?>
				        </div>
			        </div>
			    </div>
			</div>
		</div>

<!-- load js -->
<?php
	loadJS($_SESSION['levelID']);
?>

</body>
</html>

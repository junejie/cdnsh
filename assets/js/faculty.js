$(document).ready(function(){
    $("#adduser").submit(function(event){
        event.preventDefault();
        var confirm  = window.confirm('Are you sure do you want to add this user');
        if (confirm) {
            adduser();
        }
    });

    $("#usertype").on('change',function(e){
        var accnt = $(this).val();
        if (accnt == 6) {
            $("#userYear").parent().css({
                visibility:'hidden'
            });
            $("#userSection").parent().css({
                visibility:'hidden'
            });
        }else{
            $("#userYear").parent().css({
                visibility: 'visible'
            });
            $("#userSection").parent().css({
                visibility: 'visible'
            });

        }
    });

});


//add user - change year
$("#userYear").on('change',function(e){
    var id = $(this).val();
    $.ajax({
        type: "POST",
        url: "app/proc/action-user.php?action=sectionbygrade",
        data: "gradeid=" + id,
        success : function(text){
            $("select#userSection").empty().append(text);
        }
    });


});

//USER FUNCTION
function editUser(id){
    var confirm  = window.confirm('Are you sure do you want to edit this user');
    if (confirm) {
        var username = $("div#editModal"+id+" #username").val();
        var firstname = $("div#editModal"+id+" #firstname").val();
        var lastname = $("div#editModal"+id+" #lastname").val();
        var email = $("div#editModal"+id+" #email").val();
        var password = $("div#editModal"+id+" #password").val();
        var usertype = $("div#editModal"+id+" #usertype").val();
        var id = $("div#editModal"+id+" #id").val();
        $.ajax({
            type: "POST",
            url: "app/proc/action-user.php?action=edituser&id="+id,
            data: "username=" + username + "&firstname=" + firstname + "&lastname=" + lastname + "&email=" + email + "&password="+password+ "&usertype="+usertype + "&id="+id,
            success : function(text){
                if (text == "success"){
                    $("#editModal"+id).modal("hide");
                    $(".alert.alert-success").removeClass('hidden');
                    window.scrollTo(0,0);
                    setTimeout(function() { 
                        $(".alert.alert-success").addClass('hidden');
                        location.reload();
                    }, 1000);
                }else{
                    alert('Failed to add user');
                }
            }
        });
    }
}

function deleteUser(id){
    var confirm  = window.confirm('Are you sure do you want to delete this user');
    if (confirm) {
        $.ajax({
            type: "GET",
            url: "app/proc/action-user.php?action=deleteuser&id="+id,
            success : function(text){
                if (text == "success"){
                    $("#deleteModal"+id).modal("hide");
                    $(".alert.alert-success").removeClass('hidden');
                    window.scrollTo(0,0);
                    setTimeout(function() { 
                        $(".alert.alert-success").addClass('hidden');
                        location.reload();
                    }, 1000);
                }else{
                    alert("Failed to delete user")
                }
            }
        });
    }
}

function adduser(){
    var yearsection
	var username = $("#username").val();
	var firstname = $("#firstname").val();
	var lastname = $("#lastname").val();
	var email = $("#email").val();
	var password = $("#password").val();
    var usertype = $("#usertype").val();

    var useryear = $("#userYear").val();
    var usersection = $("#userSection").val();
    if (useryear != '' || userSection !='') {
        yearsection = "&useryear="+ useryear + "&usersection="+usersection;
    }else{
        yearsection = '';
    }
    $.ajax({
        type: "POST",
        url: "app/proc/action-user.php?action=adduser",
        data: "username=" + username + "&firstname=" + firstname + "&lastname=" + lastname + "&email=" + email + "&password="+password+ "&usertype="+usertype + yearsection,
        success : function(text){
            if (text == "success"){
                $("#myModal").modal("hide");
                $(".alert.alert-success").removeClass('hidden');
                window.scrollTo(0,0);
                setTimeout(function() { 
                    $(".alert.alert-success").addClass('hidden');
                    location.reload();
                }, 1000);
            }else{
            	alert('Failed to add user');
            }
        }
    });
}

$("#btnAddTBI").click( function(){
    var confirm  = window.confirm('Are you sure do you want to add tbi');
    if (confirm) {
        addTBI();
    }
})

//TBI FUNCTION
function addTBI(){
    var countOfQuestions = $("form#addtbi input[name^='question_id']");
    var data = [];
    for (var i = countOfQuestions.length - 1; i >= 0; i--) {
        q_id = countOfQuestions[i]['value']
        v = $("form#addtbi input[name='dataAnswer_"+q_id+"']").filter(':checked' ).val();
        o = {
            "id":q_id,
            "answer":v,
            "teacher":$("#teacherlist").val()
        }
        data.push(o);
    };
    $.ajax({
        type: "POST",
        url: "app/proc/action-user.php?action=addtbi",
        data: JSON.stringify(data),
        success : function(text){
            if (text == "success"){
                $(".alert.alert-success").removeClass('hidden');
                window.scrollTo(0,0);
                setTimeout(function() { 
                    $(".alert.alert-success").addClass('hidden');
                }, 1000);
            }else{
                alert('Failed to save TBI');
            }
        }
    });
}
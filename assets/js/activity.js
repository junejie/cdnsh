$(document).ready(function(){
    $('#product-table a:first').tab('show');
    $("#input-7").fileinput({
        uploadUrl: "app/proc/upload.php?action=teacherupload",
        allowedFileExtensions : ['doc','docx','ppt','pptx','xls','xlsx','pdf','txt'],
    }).on('fileuploaded', function(event, data, previewId, index) {
        var form = data.form, files = data.files, extra = data.extra,
        response = data.response, reader = data.reader;
        uploadStatus = data.jqXHR.responseJSON.status;
        uploadDocname = data.jqXHR.responseJSON.docname;
        uploadFilename = data.jqXHR.responseJSON.filename;
    });

    $("form[name='frmUpdateactivity']").submit(function(event){
        event.preventDefault();
        e = $(this)
        var confirm  = window.confirm('Do you want to update this activity');
        if (confirm) {
            updateActivity()
        }
    });

    //auto populate 
    $("#activityUserYear").on('change',function(){
        var id = $(this).val();
        //POPULATE GRADE 
        $.ajax({
            type: "POST",
            url: "app/proc/action-user.php?action=sectionbygradesched",
            data: "gradeid=" + id,
            success : function(text){
                $("select#activityUserSection").empty().append(text);
            }
        }); 

    });
});

$("#frmAddactivity").submit(function(event){
    event.preventDefault();
    var confirm  = window.confirm('Do you want to add this activity');
    if (confirm) {
        addActivity();
    }
});


/*-- valid upload --*/
function acf_validateUpload() {
    validateUpload = setInterval(function(){ 
        if (uploadStatus) {
            var confirm  = window.confirm('Do you want to save this activity');
            if (confirm) {
                acf_validateUpload_stop();
                saveActivity();
                uploadStatus = false;
            }    
        }
    }, 1000);
}
function acf_validateUpload_stop() {
    clearTimeout(validateUpload);
}

//ADD activity
var saveActivity = function(){
    var activityUserYear = $("#activityUserYear").val();
    var activityUserSection = $("#activityUserSection").val();
    var activitydate = $("#activitydate").val();
    var activitycontent = $("#activitycontent").val();
    var activitystatus = $("#activitystatus").val();
    var filedata = "&uploadDocname="+uploadDocname+"&uploadFilename="+uploadFilename;
    $.ajax({
        type: "POST",
        url: "app/proc/action-user.php?action=addactivity",
        data: "activityUserYear=" + activityUserYear + "&activityUserSection=" + activityUserSection + "&activitydate=" + activitydate + "&activitycontent=" + activitycontent+ "&activitystatus=" + activitystatus + filedata ,
        success : function(text){
            if (text == "success"){
                $("#addactivity").modal("hide");
                $(".alert.alert-success").removeClass('hidden');
                window.scrollTo(0,0);
                setTimeout(function() { 
                    $(".alert.alert-success").addClass('hidden');
                    location.reload();
                }, 1000);
            }else{
                alert('Failed to save activity');
            }
        }
    });
}
function addActivity(){
    console.log('add ac')
    var isUploaded = $(".progress-bar.progress-bar-success").attr('aria-valuenow');
    if ( isUploaded == '100') {
        saveActivity();
    } else{
        var isFileSelected = $(".file-caption-name").attr('title');
        var isFileSelected1 = $(".file-preview-frame").attr('title');
        if ( isFileSelected != '' && isFileSelected1 != undefined ) {
            //force click upload
            $("a.btn.btn-default.fileinput-upload.fileinput-upload-button").click();
            acf_validateUpload();
        } else{
            if ( isFileSelected1 == 'undefined') {
                saveActivity();
            } else{
                saveActivity();
            };
        };
    };
}

function updateActivity(){
    var id = $(".modal.fade.in").find("input[type='hidden']").val()
    var status = $(".modal.fade.in").find("select#activitystatus").val()
    var data = {
        'id': id,
        'status': status
    }
    
    $.ajax({
        type: "POST",
        url: "app/proc/action-user.php?action=updateactivity",
        data:  data,
        success : function(text){
            if (text == "success"){
                $("#act_"+id).modal("hide");
                $(".alert.alert-success").removeClass('hidden');
                window.scrollTo(0,0);
                setTimeout(function() { 
                    $(".alert.alert-success").addClass('hidden');
                    //location.reload();
                }, 1000);
            }else{
                alert('Failed to update activity');
            }
        }
    });

}

//DELETE Activity
function deleteActivity(id) {
    $.ajax({
        type: "POST",
        url: "app/proc/action-user.php?action=deleteactivity",
        data: "id=" + id ,
        success : function(text){
            if (text == "success"){
                $("#deleteActivity"+id).modal("hide");
                $(".alert.alert-success").removeClass('hidden');
                window.scrollTo(0,0);
                setTimeout(function() { 
                    $(".alert.alert-success").addClass('hidden');
                    location.reload();
                }, 1000);
            }else{
                alert('Failed to delete activity');
            }
        }
    });
}

// POPULATE LIST OF STUDENT SUBMITTED DOCS
function viewActivitiesFromStudent(id){
    $.ajax({
        type: "GET",
        url: "app/proc/action-user.php?action=getstudentactivities&activity="+id,
        success : function(text){
            $("#divtable-"+id).empty().append(text);
        }
    });
}
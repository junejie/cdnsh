$(document).ready(function() {
    $('#calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        defaultDate: new Date(),
        selectable: true,
        selectHelper: true,
        eventRender: function(event, element) {
            if(event.type_calendar != 'a'){
                element.append( '<span class=" glyphicon glyphicon-remove closeon"></span>' );
            }

            // delete
            element.find(".closeon").click(function() {
                var confirm  = window.confirm('Are you sure do you want to delete event');
                if (confirm) {
                    $.ajax({
                        url: "app/proc/action-user.php?action=deleteevents&id="+event._id,
                        type:"GET",
                        complete: function(e){
                            if(e.status != 200){
                                alert("Failed to delete");
                            }

                        }
                    });
                   $('#calendar').fullCalendar('removeEvents',event._id);
                }
            });

            //click detail
            element.attr('href', 'javascript:void(0);');
            element.click(function() {
                $("#startTime").html(moment(event.start).format('MMM Do h:mm A'));
                $("#endTime").html(moment(event.end).format('MMM Do h:mm A'));
                $("#eventInfo").html(event.description);
                $("#eventID").html(event.content);
                $("#eventLink").attr('href', event.url);
                $("#eventContent").dialog({ 
                    modal: true, 
                    title: event.title, 
                    content: event.content,
                    width:350
               });
            });


        },
        select: function(start, end) {
            var dialog = $( "#dialog" ).dialog({
                autoOpen: false,
                height: 300,
                width: 350,
                modal: true,
                buttons: {
                    "Add Event": function(){

                        var eventData;
                        eventData = {
                            'title': $("#c_title").val(),
                            'content': $("#c_content").val(),
                            'start': start,
                            'end': end
                        };

                        //save..
                        $.ajax({
                            type: "POST",
                            url: "app/proc/action-user.php?action=addevents",
                            dataType: "json",
                            data: JSON.stringify( eventData),
                            complete: function(e){
                                $('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
                                dialog.dialog( "close" );
                            }
                        });
                        // ----
                    },
                    Cancel: function() {
                    dialog.dialog( "close" );
                    }
                },
                close: function() {
                    $("#c_title").val('');
                    $("#c_content").val('');
                }
            });
            dialog.dialog('open');
            
            $('#calendar').fullCalendar('unselect');
        },
        editable: true,
        eventLimit: true, // allow "more" link when too many events
        eventSources: [{
                url: 'app/proc/action-user.php?action=getevents',
                type: 'POST'
        }]
    });
});
$(document).ready(function(){
    $('#btnAddGrade').click( function(){
        var confirm  = window.confirm('Are you sure do you want to add this grade');
        if (confirm) {
            addgrade();
        }
    }); 

    $("#btnSearchGrade").click(function(){
        searchGrade();
    });

    $("#dataYear").on('change',function(e){
        var id = $(this).val();
        //POPULATE GRADE 
        $.ajax({
            type: "POST",
            url: "app/proc/action-user.php?action=sectionbygradesched",
            data: "gradeid=" + id,
            success : function(text){
                $("select#dataSection").empty().append(text);
            }
        });

        //POPULATE SUBJECT
        $.ajax({
            type: "POST",
            url: "app/proc/action-user.php?action=subjectbygradesched",
            data: "gradeid=" + id,
            success : function(text){
                $("select#dataSubject").empty().append(text)
            }
        });

    });

    //GRADE FUNCTION
    function addgrade(){
        var subject = $("#dataSubject").val()
        var g0 = $( "input[name^='g0_']" );
        var g1 = $("span[name^='g1_']")
        var g2 = $("span[name^='g2_']")
        var g3 = $("span[name^='g3_']")
        var g4 = $("span[name^='g4_']")

        data = [];
        g0.each(function(key,elem){
            o = {
                "subject": subject,
                "student":elem.value,
                "g1": g1[key].innerHTML,
                "g2": g2[key].innerHTML,
                "g3": g3[key].innerHTML,
                "g4": g4[key].innerHTML
            };
            data.push(o)
        });
        $.ajax({
            type: "POST",
            url: "app/proc/action-user.php?action=savegrade",
            data: JSON.stringify(data),
            success : function(text){
                console.log(text)
                if (text == "success"){
                    $("#myModal").modal("hide");
                    $(".alert.alert-success").removeClass('hidden');
                    window.scrollTo(0,0);
                    setTimeout(function() { 
                        $(".alert.alert-success").addClass('hidden');
                        //location.reload();
                    }, 1000);
                }else{
                    alert('Failed to add user');
                }
            }
        });
    }

    //GRADE SEARCH
    function searchGrade(){
        var dataYear = $("#dataYear").val();
        var dataSection = $("#dataSection").val();
        var dataSubject = $("#dataSubject").val();

        $.ajax({
            type: "POST",
            url: "app/form/grade-search.php",
            data: "dataYear=" + dataYear + "&dataSection=" + dataSection + "&dataSubject=" + dataSubject ,
            success : function(text){
                $("#addgrade").remove();
                $("#grade-tab").append(text);

                //REBIND
                $('#btnAddGrade').click( function(){
                    addgrade();
                }); 
            }
        });
    }

    //GRADE CHAR LIMIT
    var limit = 3;
    $('span[contenteditable]').keypress(function() {
        return this.innerHTML.length < limit;
    }).on({
        'paste': function(e) {
            var len = this.innerHTML.length,
            cp = e.originalEvent.clipboardData.getData('text');
            if (len < limit)
                this.innerHTML += cp.substring(0, limit - len);
            return false;
        },
        'drop': function(e) {
            e.preventDefault();
            e.stopPropagation();
        }
    });
});
$(document).ready(function(){
    $('#product-table a:first').tab('show');
    $("#inputlecture-7").fileinput({
        uploadUrl: "app/proc/upload.php?action=teacheruploadlecture",
        allowedFileExtensions : ['doc','docx','ppt','pptx','xls','xlsx','pdf','txt']
    }).on('fileuploaded', function(event, data, previewId, index) {
        var form = data.form, files = data.files, extra = data.extra,
        response = data.response, reader = data.reader;
        uploadStatus = data.jqXHR.responseJSON.status;
        uploadDocnameLecture = data.jqXHR.responseJSON.docname;
        uploadFilenameLecture = data.jqXHR.responseJSON.filename;
    });

    $("form[name='frmUpdateactivity']").submit(function(event){
        var confirm  = window.confirm('Are you sure do you want to update this activity');
        if (confirm) {
            event.preventDefault();
            e = $(this)
            updateActivity()
        }
    });
});

$("#frmAddLecture").submit(function(event){
    var confirm  = window.confirm('Are you sure do you want to add this lecture');
    if (confirm) {
        event.preventDefault();
        addLecture();
    }
});


/*-- valid upload --*/
function f_validateUpload() {
    validateUpload = setInterval(function(){ 
        if (uploadStatus) {
            f_validateUpload_stop();
            saveLecture();
            uploadStatus = false;
        }
    }, 1000);
}
function f_validateUpload_stop() {
    clearTimeout(validateUpload);
}

//ADD lecture
var saveLecture = function(){
    var lectureUserYear = $("#lectureUserYear").val();
    var lectureUserSection = $("#lectureUserSection").val();
    var lecturedate = $("#lecturedate").val();
    var lecturecontent = $("#lecturetitle").val();
    var lectureSubject = $("#lectureSubject").val()

    //validate
    if(lecturedate == ''){
        alert('Please enter date.');
        console.log(111)
        return;
    };
    if (lecturecontent == '') {
        alert('Please enter title');
        return;
    }
    if (
        typeof uploadDocnameLecture != 'undefined' ||
        typeof uploadFilenameLecture != 'undefined' ) {  

        var filedata = "&uploadDocnameLecture="+uploadDocnameLecture+"&uploadFilenameLecture="+uploadFilenameLecture;
        data = "lectureUserYear=" + lectureUserYear + "&lectureUserSection=" + lectureUserSection + "&lecturedate=" + lecturedate + "&lecturecontent=" + lecturecontent + "&lectureSubject="+lectureSubject + filedata ,
        $.ajax({
            type: "POST",
            url: "app/proc/action-user.php?action=addlecture",
            data: data,
            success : function(text){
                if (text == "success"){
                    $("#addLecture").modal("hide");
                    $(".alert.alert-success").removeClass('hidden');
                    window.scrollTo(0,0);
                    setTimeout(function() { 
                        $(".alert.alert-success").addClass('hidden');
                        location.reload();
                    }, 1000);
                }else{
                    alert('Failed to save Lecture');
                }
            }
        });
    } else {

        alert('Plese upload document');
    }
}

function addLecture(){
    var isUploaded = $(".progress-bar.progress-bar-success").attr('aria-valuenow');
    if ( isUploaded == '100') {
        saveLecture();
    } else{
        var isFileSelected = $(".file-caption-name").attr('title');
        var isFileSelected1 = $(".file-preview-frame").attr('title');
        if ( isFileSelected != '' && isFileSelected1 != undefined ) {
            //force click upload
            $("a.btn.btn-default.fileinput-upload.fileinput-upload-button").click();
            f_validateUpload();
        } else{
            if ( isFileSelected1 == 'undefined') {
                saveLecture();
            } else{
                saveLecture();
            };
        };
    };
}

function onChangeYear(){
    var year = $("#lectureUserYear").val();
    var data = {
        "year": year
    }
    $.ajax({
        type: "POST",
        url: "app/proc/action-user.php?action=getsubject",
        data:  data,
        success : function(option){
            $("#lectureSubject").empty().append(option)
        }
    });

    //POPULATE section 
        $.ajax({
            type: "POST",
            url: "app/proc/action-user.php?action=sectionbygrade",
            data: "gradeid=" + year,
            success : function(text){
                $("select#lectureUserSection").empty().append(text);
            }
        });
}

// function updateActivity(){
//     var id = $(".modal.fade.in").find("input[type='hidden']").val()
//     var status = $(".modal.fade.in").find("select#activitystatus").val()
//     var data = {
//         'id': id,
//         'status': status
//     }
    
//     $.ajax({
//         type: "POST",
//         url: "app/proc/action-user.php?action=updateactivity",
//         data:  data,
//         success : function(text){
//             if (text == "success"){
//                 $("#act_"+id).modal("hide");
//                 $(".alert.alert-success").removeClass('hidden');
//                 window.scrollTo(0,0);
//                 setTimeout(function() { 
//                     $(".alert.alert-success").addClass('hidden');
//                     //location.reload();
//                 }, 1000);
//             }else{
//                 alert('Failed to update activity');
//             }
//         }
//     });

// }

//deleteLecture
function deleteLecture(id) {
    var confirm  = window.confirm('Are you sure do you want to delete this lecture');
    if (confirm) {
        $.ajax({
            type: "POST",
            url: "app/proc/action-user.php?action=deletelecture",
            data: "id=" + id ,
            success : function(text){
                if (text == "success"){
                    $("#deleteLecture"+id).modal("hide");
                    $(".alert.alert-success").removeClass('hidden');
                    window.scrollTo(0,0);
                    setTimeout(function() { 
                        $(".alert.alert-success").addClass('hidden');
                        location.reload();
                    }, 1000);
                }else{
                    alert('Failed to delete activity');
                }
            }
        });
    }
}

// // POPULATE LIST OF STUDENT SUBMITTED DOCS
// function viewActivitiesFromStudent(id){
//     $.ajax({
//         type: "GET",
//         url: "app/proc/action-user.php?action=getstudentactivities&activity="+id,
//         success : function(text){
//             $("#divtable-"+id).empty().append(text);
//         }
//     });
// }
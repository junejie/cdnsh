$(document).ready(function(){

    $("#frmAddAnnouncement").submit(function(event){
        event.preventDefault();
        var confirm  = window.confirm('Are you sure do you want to add announcement');
        if (confirm) {
            addAnnouncement();
        }
    });

    //on change
    $("#announcementUserYear").on('change', function(e){
        var id = $(this).val();
        //POPULATE GRADE 
        $.ajax({
            type: "POST",
            url: "app/proc/action-user.php?action=sectionbygrade",
            data: "gradeid=" + id,
            success : function(text){
                $("select#announcementUserSection").empty().append(text);
            }
        });
    });
});


//ADD ANNOUNCEMENT
function addAnnouncement(){
    var announcementUserYear = $("#announcementUserYear").val();
    var announcementUserSection = $("#announcementUserSection").val();
    var announcementdate = $("#announcementdate").val();
    var announcementcontent = $("#announcementcontent").val();
    $.ajax({
        type: "POST",
        url: "app/proc/action-user.php?action=addannouncement",
        data: "announcementUserYear=" + announcementUserYear + "&announcementUserSection=" + announcementUserSection + "&announcementdate=" + announcementdate + "&announcementcontent=" + announcementcontent ,
        success : function(text){
            console.log(text)
            if (text == "success"){
                $("#addAnnouncement").modal("hide");
                $(".alert.alert-success").removeClass('hidden');
                window.scrollTo(0,0);
                setTimeout(function() { 
                    $(".alert.alert-success").addClass('hidden');
                    location.reload();
                }, 1000);
            }else{
                alert('Failed to add user');
            }
        }
    });
}



//DEL ANNOUNCEMENT
function delAnnouncement(id){
    var confirm  = window.confirm('Are you sure do you want to add announcement');
    if (confirm) {
        $.ajax({
            type: "POST",
            url: "app/proc/action-user.php?action=delannouncement",
            data: "id=" + id  ,
            success : function(text){
                console.log(text)
                if (text == "success"){
                    $("#deleteModalAnnouncement"+id).modal('hide')
                    $(".alert.alert-success").removeClass('hidden');
                    window.scrollTo(0,0);
                    setTimeout(function() { 
                        $(".alert.alert-success").addClass('hidden');
                        location.reload();
                    }, 1000);
                }else{
                    alert('Failed to delete announcement');
                }
            }
        });
    }
}
$(document).ready(function(){
    //upload my activities
    //reloadUploader();
});


$("#btnAddTBI").click( function(){
    var confirm  = window.confirm('Are you sure do you want to add this TBI');
    if (confirm) {
        addTBI();
    }
});

function reloadUploader(id){
    $('input[type="file"]').fileinput({
        uploadUrl: "app/proc/upload.php?action=studentupload",
        allowedFileExtensions : ['doc','docx','ppt','pptx','xls','xlsx','pdf','txt'],
        uploadExtraData:{
            "d1":id
        }
    //SEND DATA BEFORE UPLOAD
    }).on('filepreupload', function(event, data, previewId, index) {
        var form = data.form, files = data.files, extra = data.extra,
            response = data.response, reader = data.reader;
    
    //FINALLY UPLOAD
    }).on('fileuploaded', function(event, data, previewId, index) {
        var form = data.form, files = data.files, extra = data.extra,
        response = data.response, reader = data.reader;
        uploadStatus = data.jqXHR.responseJSON.status;
        if ( uploadStatus == 'true') {
            $("#actupload_0").modal('hide')
            $(".alert.alert-success").removeClass('hidden');
            window.scrollTo(0,0);
            setTimeout(function() { 
                $(".alert.alert-success").addClass('hidden');
            }, 1000);
        } else{
            alert('Failed to upload.');
        };
    });
}

//TBI FUNCTION
function addTBI(){
    var countOfQuestions = $("form#addtbi input[name^='question_id']");
    var data = [];
    for (var i = countOfQuestions.length - 1; i >= 0; i--) {
        q_id = countOfQuestions[i]['value']
        v = $("form#addtbi input[name='dataAnswer_"+q_id+"']").filter(':checked' ).val();

        // IF ONLY VALUE HAS DATA
        if(typeof v == 'string'){
            o = {
                "id":q_id,
                "answer":v,
                "teacher":$("#teacherlist").val()
            }
            data.push(o);
        }
    };
    if(data.length === countOfQuestions.length){
        $.ajax({
            type: "POST",
            url: "app/proc/action-user.php?action=addtbi",
            data: JSON.stringify(data),
            success : function(text){
                if (text == "success"){
                    $(".alert.alert-success").removeClass('hidden');
                    window.scrollTo(0,0);
                    setTimeout(function() { 
                        $(".alert.alert-success").addClass('hidden');
                    }, 1000);
                }else{
                    alert('Failed to save TBI');
                }
            }
        });
    }else{
        alert('Please complete TBI');
    }
}
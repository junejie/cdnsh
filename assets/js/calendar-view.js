$(document).ready(function() {
    $('#calendar-view').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        defaultDate: new Date(),
        selectable: false,
        selectHelper: true,
        eventRender: function(event, element) {
            //click detail
            element.attr('href', 'javascript:void(0);');
            element.click(function() {
                $("#startTime").html(moment(event.start).format('MMM Do h:mm A'));
                $("#endTime").html(moment(event.end).format('MMM Do h:mm A'));
                $("#eventInfo").html(event.description);
                $("#eventID").html(event.content);
                $("#eventLink").attr('href', event.url);
                $("#eventContent").dialog({ 
                    modal: true, 
                    title: event.title, 
                    content: event.content,
                    width:350
               });
            });
        },
        editable: false,
        eventLimit: true, // allow "more" link when too many events
        eventSources: [{
                url: 'app/proc/action-user.php?action=getevents',
                type: 'POST'
        }]
    });
});
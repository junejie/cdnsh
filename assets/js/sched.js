var grid, dialog;
$(document).ready(function () {
    $('#starttime').timepicker({ 'timeFormat': 'H:i:s' });
    $('#endtime').timepicker({ 'timeFormat': 'H:i:s' });

    c_data = function(elem){
        //POPULATE SCHED
        $.ajax({
            type: "POST",
            url: "app/proc/action-user.php?action=getschedule",
            data: "gradeid=" + 1+"&t="+elem,   
            success : function(text){
                data = text;
                dialog = $("#dialog").dialog({
                    title: "Add/Edit Record",
                    autoOpen: false,
                    resizable: false,
                    width: 600,
                    height: 400,
                    modal: true,
                    buttons: {
                        "Save": Save,
                        "Cancel": function () { $(this).dialog("close"); }
                    }
                });
                function Edit(e) {
                    //e.data.id = e.data.record.ID;
                    var teacherid = $("#select-sched-teacher").val();
                    if (teacherid != "") {
                        var gradeid = e.data.record.gradeid;
                        var sectionid = e.data.record.sectionid;
                        var subject = e.data.record.subject;
                        $('#grade option[value="'+gradeid+'"]').attr('selected','true')
                        $("#ID").val(e.data.id);
                        $("#sched-id").val(e.data.record.ID);
                        $("#starttime").val(e.data.record.starttime);
                        $("#endtime").val(e.data.record.endtime);
                        $("#subject").val(subject);

                        //POPULATE SECTION AND SUBJECT BY GRADE
                        //POPULATE GRADE 
                        $.ajax({
                            type: "POST",
                            url: "app/proc/action-user.php?action=sectionbygrade",
                            data: "gradeid=" + gradeid,
                            success : function(text){
                                $("select#section").empty().append(text);
                                $('#section option[value="'+sectionid+'"]').attr('selected','true')
                            }
                        });

                        //POPULATE SUBJECT
                        $.ajax({
                            type: "POST",
                            url: "app/proc/action-user.php?action=subjectbygrade",
                            data: "gradeid=" + gradeid,
                            success : function(text){
                                $("select#subject").empty().append(text)
                                $('#subject option[value="'+subject+'"]').attr('selected','true')
                            }
                        });

                        //SET VALUE AFTER OPTION HAS BEEN SET.
                        //SET SELECTED OPTION

                        $("#dialog").dialog("open");
                    
                    } else {
                        alert("Failed to update, Please select Teacher");
                    }
                }
                function Delete(e) {
                    if (confirm("Are you sure?")) {
                        $.ajax({
                            type: "POST",
                            url: "app/proc/action-user.php?action=deleteschedule",
                            data: "id="+e.data.record.ID,
                            success : function(text){
                                var status = text[0]['status'];
                                if (status == "success") {
                                    grid.removeRow(e.data.id);
                                } else {
                                    alert('Failed to delete schedule')
                                }
                            }
                        });
                    }
                }
                function Save() {
                    var confirm  = window.confirm('Are you sure do you want to add this schedule');
                    if (confirm) {
                        var i = $("#sched-id").val()
                        if ($("#ID").val()) {
                            var id = parseInt($("#ID").val());
                            var teacherid = $("#select-sched-teacher").val();
                            var gradeid = $("select#grade").val();
                            var sectionid = $("select#section").val();
                            var subjectid = $("select#subject").val();
                            var starttime = $("#starttime").val();
                            var endtime = $("#endtime").val();
                            $.ajax({
                                type: "POST",
                                url: "app/proc/action-user.php?action=updateschedule",
                                data: "id="+i+"&teacherid="+teacherid+"&gradeid=" + gradeid + "&sectionid="+sectionid+"&subjectid="+subjectid+"&starttime="+starttime+"&endtime="+endtime,
                                success : function(text){
                                    grid.updateRow(
                                        id, 
                                        { 
                                            "ID": id, 
                                            "starttime": $("#starttime").val(), 
                                            "endtime": $("#endtime").val(),
                                            "gradeid": $("select#grade").val(),
                                            "subjectname": $("#subject :selected").text() ,
                                            "sectionname": $("#section :selected").text(),
                                            "gradename": $("#grade :selected").text()
                                        } );
                                }
                            });


                        } else {

                            var teacherid = $("#select-sched-teacher").val();
                            var gradeid = $("select#grade").val();
                            var sectionid = $("select#section").val();
                            var subjectid = $("select#subject").val();
                            var starttime = $("#starttime").val();
                            var endtime = $("#endtime").val();
                            $.ajax({
                                type: "POST",
                                url: "app/proc/action-user.php?action=addschedule",
                                data: "teacherid="+teacherid+"&gradeid=" + gradeid + "&sectionid="+sectionid+"&subjectid="+subjectid+"&starttime="+starttime+"&endtime="+endtime,
                                success : function(text){
                                    var c_d = text[0]['id'];
                                    grid.addRow(
                                        { 
                                            "ID": c_d,
                                            "starttime": starttime, 
                                            "endtime": endtime,
                                            "gradeid":gradeid,
                                            "sectionid":sectionid,
                                            "subject":subjectid,
                                            "subjectname": $("#subject :selected").text(),
                                            "sectionname": $("#section :selected").text(),
                                            "gradename": $("#grade :selected").text()
                                        }
                                    );
                                }
                            });
                        }
                        $(this).dialog("close");
                    }
                }
                grid = $("#grid").grid({
                    dataSource: data,
                    columns: [
                        { field: "gradename", title: "Grade",width:200 },
                        { field: "sectionname", title: "Section",width:200 },
                        { field: "subjectname", title: "Subject",width:200 },
                        { field: "starttime",title:"Start Time",width:100 },
                        { field: "endtime", title: "End Time", width:100 },
                        { title: "Edit", width: 20, type: "icon", icon: "ui-icon-pencil", tooltip: "Edit", events: { "click": Edit } },
                        { title: "Delete", width: 20, type: "icon", icon: "ui-icon-close", tooltip: "Delete", events: { "click": Delete } }
                    ]
                });
                grid.render(data);

                
                $("#btnAddSched").on("click", function () {
                    var teacher = $("#select-sched-teacher").val();
                    if (teacher == '') {
                        $("#select-sched-teacher").addClass('select-invalid')
                    } else {
                        $("#select-sched-teacher").removeClass('select-invalid')
                        $("#ID").val("");
                        $("#starttime").val("");
                        $("#endtime").val("");
                        $("#subject").val("");
                        $("#dialog").dialog("open");
                    }
                });

                //onchange of grade in sched tab
                $("#grade").on('change',function(){ 
                    var gradeid = $("select#grade").val();
                    //POPULATE GRADE 
                    $.ajax({
                        type: "POST",
                        url: "app/proc/action-user.php?action=sectionbygrade",
                        data: "gradeid=" + gradeid,
                        success : function(text){
                            $("select#section").empty().append(text)
                        }
                    });

                    //POPULATE SUBJECT
                    $.ajax({
                        type: "POST",
                        url: "app/proc/action-user.php?action=subjectbygrade",
                        data: "gradeid=" + gradeid,
                        success : function(text){
                            $("select#subject").empty().append(text)
                        }
                    });
                });
            }
        });
    }
    c_data(0);
});

$("#select-sched-teacher").on('change',function(e){
    var e = $(this).val();
    grid.reload();
    c_data(e);
});
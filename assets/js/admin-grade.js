var demo2 = $('.select-setting-grade').bootstrapDualListbox({
  nonSelectedListLabel: 'Deactivated',
  selectedListLabel: 'Activated',
  preserveSelectionOnMove: 'moved',
  moveOnSelect: false
});


//GRADE SETTING WRENCH
$("#btnGradeSetting").click(function(){
    $("#grade-setting").toggle();
});

//CANCEL SETTING
$("#grade-setting-cancel").click(function(){
    $("#grade-setting").hide();
});

$("#grade-setting-save").click(function(){
    
    //SELECTED DATA
    var selectedItem = $("select[name='activeteacher_helper2'] option");
    finalArr = []
    selectedItem.each( function(currentElem){
        finalArr.push(selectedItem[currentElem].value)
    });

    //NON SELECTED DATA
    var selectedItem = $("select[name='activeteacher_helper1'] option");
    data_nonselected = []
    selectedItem.each( function(currentElem){
        data_nonselected.push(selectedItem[currentElem].value)
    });
    
    
    $.ajax({
        type: "POST",
        url: "app/proc/action-user.php?action=gradesetting",
        data:  { 
            "data":finalArr,
            "data_nonselected": data_nonselected
        },
        success : function(text){
            if (text == "success"){
                $("#grade-setting").hide();
                $(".alert.alert-success").removeClass('hidden');
                window.scrollTo(0,0);
                setTimeout(function() { 
                    $(".alert.alert-success").addClass('hidden');
                }, 1000);
            }else{
                alert('Failed to update activity');
            }
        }
    });
    
});
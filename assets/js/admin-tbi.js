//CLICK SETTING
$("#tbi_setting").click(function(){
	$("#tbiSetting div.alert").show();
	$("button#tbi_setting").hide();
});

$("#tbisettingcancel").click(function(){
	$("#tbiSetting div.alert").hide();
	$("button#tbi_setting").show();
});

$("#tbisettingsave").click(function(){
    console.log('save');
    var confirm  = window.confirm('Are you sure do you want to save tbi setting');
    if ( confirm ) {
        var tbisy = $("#tbisy").val();
        var tbi_setting_student = $("#tbi_setting_student").is(":checked");
        var tbi_setting_teacher = $("#tbi_setting_teacher").is(":checked");
        $.ajax({
            type: "POST",
            url: "app/proc/action-user.php?action=tbisetting",
            data: "tbisy=" + tbisy + "&tbi_setting_student=" + tbi_setting_student + "&tbi_setting_teacher=" + tbi_setting_teacher,
            success : function(text){
                if (text == "success"){
                    $(".alert.alert-success").removeClass('hidden');
                    window.scrollTo(0,0);
                    setTimeout(function() { 
                        $(".alert.alert-success").addClass('hidden');
                        //location.reload();
                    }, 1000);
                }else{
                    alert('Failed to Save Setting.');
                }
            }
        });
    }
});


<nav class="navbar navbar-default">
	<div class="container-fluid">
		<!-- superuser -->
		<?php
			//var_dump(  $_SESSION); 
			$currentFolder = md5($_SESSION['id']);
			$save_folder = 'userfiles/'.$currentFolder;
			if (!file_exists($save_folder)) {
				mkdir($save_folder, 0777);
			}
			$nameLabel = $_SESSION['user_type']  .' | ' . $_SESSION['fullname'];

			$currentFolder;
			if($_SESSION['levelID']==7){ ?>
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			   <a class="navbar-brand" href="main.php">Casa Del Niño Schools System Inc. </a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			  <ul class="nav navbar-nav">
				<li><a href="users.php"><span class="glyphicon glyphicon-home"></span> Home</a></li>
			  </ul>
			  <ul class="nav navbar-nav navbar-right">
				<li class="dropdown">
				  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?=$nameLabel;?><span class="caret"></span></a>
				  <ul class="dropdown-menu">
					
					<li><a href="users.php">Users</a></li>
					<li><a href="index.php">Logout</a></li>
				  </ul>
				</li>
			  </ul>
			</div><!-- /.navbar-collapse -->
		<?php }?>

		<!-- principal -->
		<?php if($_SESSION['levelID']==1){ 
			$nameLabel = $_SESSION['user_gradelabel'] .' '.$_SESSION['user_sectionlabel']   .' | ' . $_SESSION['fullname'];
		?>
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			   <a class="navbar-brand" href="main.php">Casa Del Niño Schools System Inc. </a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			  <ul class="nav navbar-nav">
				<li><a href="main.php"><span class="glyphicon glyphicon-home">
					
				</span> Home</a></li>
				<li><a href="student.php"><span class="glyphicon glyphicon-flag">
					
				</span> Student</a> </li>
				<li><a href="dropbox.php?exclusiveFolder=<?=$currentFolder?>"><span class="glyphicon glyphicon-folder-open">
					
				</span>  File Manager</a></li>
			  </ul>
			  <ul class="nav navbar-nav navbar-right">
				<li class="dropdown">
				  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?=$nameLabel;?><span class="caret"></span></a>
				  <ul class="dropdown-menu">
					<li><a href="index.php">Logout</a></li>
				  </ul>
				</li>
			  </ul>
			</div><!-- /.navbar-collapse -->
		<?php }?>
		

		<!-- admin -->
		<?php if($_SESSION['levelID']==2){ ?>
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			  <a class="navbar-brand" href="main.php">Casa Del Niño Schools System Inc. </a>
			</div>
	
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			  <ul class="nav navbar-nav">
				<li>
					<a href="main.php">
					<span class="glyphicon glyphicon-home"></span> Home</a>
					</li>
				<li>
					<a href="it-admin.php">
					<span class="glyphicon glyphicon-tasks"></span> Setting</a></li>
			  </ul>
			  <ul class="nav navbar-nav navbar-right">
				<li>
					<a href="#"> <?php echo "Today is " . date("m/d/Y"). " " . date("h:i:a"); ?> </a></li>
				<li class="dropdown">
				  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-user"></span>  <?=$nameLabel;?><span class="caret"></span></a>
				  <ul class="dropdown-menu">
					<li role="separator" class="divider"></li>
					<li><a href="index.php">Logout</a></li>
				  </ul>
				</li>
			  </ul>
			</div><!-- /.navbar-collapse -->
		<?php }?>

		<!-- a-itprof -->
		<?php if($_SESSION['levelID']==3){ ?>
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			  <a class="navbar-brand" href="main.php">Casa Del Niño Schools System Inc. </a>
			</div>
	
			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			  <ul class="nav navbar-nav">
				<li>
					<a href="main.php">
					<span class="glyphicon glyphicon-home"></span> Home</a>
					</li>
				<li>
					<a href="it-admin.php">
					<span class="glyphicon glyphicon-tasks"></span> Setting</a></li>
			  </ul>
			  <ul class="nav navbar-nav navbar-right">
				<li>
					<a href="#"> <?php echo "Today is " . date("m/d/Y"). " " . date("h:i:a"); ?> </a></li>
				<li class="dropdown">
				  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-user"></span>  <?=$nameLabel;?><span class="caret"></span></a>
				  <ul class="dropdown-menu">
					<li role="separator" class="divider"></li>
					<li><a href="index.php">Logout</a></li>
				  </ul>
				</li>
			  </ul>
			</div><!-- /.navbar-collapse -->
		<?php }?>

		<!-- teacher2 NAVIGATION -->
		<?php if($_SESSION['levelID']==4){ ?>
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			   <a class="navbar-brand" href="main.php">Casa Del Niño Schools System Inc. </a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			  <ul class="nav navbar-nav">
				<li><a href="main.php"><span class="glyphicon glyphicon-home"></span> Home</a></li>
				<li><a href="it-prof.php"><span class="glyphicon glyphicon-flag"></span>Manage</a> </li>
			  </ul>
			  <ul class="nav navbar-nav navbar-right">
				<li>
					<a href="#"> <?php echo "Today is " . date("m/d/Y"). " " . date("h:i:a"); ?> </a></li>
				<li class="dropdown">
				  <a href="#" class="dropdown-toggle" 
				  data-toggle="dropdown" role="button" 
				  aria-haspopup="true" aria-expanded="false">
				  <span class="glyphicon glyphicon-user"></span>  
				  <?=$nameLabel;?>
				  <span class="caret"></span></a>
				  <ul class="dropdown-menu">
					<li role="separator" class="divider"></li>
					<li><a href="index.php">Logout</a></li>
				  </ul>
				</li>
			  </ul>
			</div><!-- /.navbar-collapse -->
		<?php }?>
		

		<!-- registrar -->
		<?php if($_SESSION['levelID']==5){ ?>
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			   <a class="navbar-brand" href="main.php">Casa Del Niño Schools System Inc. </a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			  <ul class="nav navbar-nav">
				<li><a href="main.php"><span class="glyphicon glyphicon-home"></span> Home</a></li>
				<li><a href="registrar.php"><span class="glyphicon glyphicon-flag"></span> Registrar</a> </li>
			  </ul>
			  <ul class="nav navbar-nav navbar-right">
				<li class="dropdown">
				  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?=$nameLabel;?><span class="caret"></span></a>
				  <ul class="dropdown-menu">
					<li><a href="index.php">Logout</a></li>
				  </ul>
				</li>
			  </ul>
			</div><!-- /.navbar-collapse -->
		<?php }?>
	
		<!-- it prof faculty -->
		<?php if($_SESSION['levelID']==6){ ?>
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			   <a class="navbar-brand" href="main.php">Casa Del Niño Schools System Inc. </a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			  <ul class="nav navbar-nav">
				<li>
				<a href="main.php">
				<span class="glyphicon glyphicon-home">
				</span> Home
				</a></li>
				<li><a href="it-prof.php">
				<span class="glyphicon glyphicon-flag"></span>IT Prof</a> </li>
			  </ul>
			  <ul class="nav navbar-nav navbar-right">
				<li class="dropdown">
				  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?=$nameLabel;?><span class="caret"></span></a>
				  <ul class="dropdown-menu">
					<li><a href="index.php">Logout</a></li>
				  </ul>
				</li>
			  </ul>
			</div><!-- /.navbar-collapse -->
		<?php }?>

		
	</div><!-- /.container-fluid -->
</nav>